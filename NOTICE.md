# Notices for Eclipse openK User Modules

This content is produced and maintained by the Eclipse openK User Modules
project.

* Project home:
   https://projects.eclipse.org/projects/technology.openk-usermodules

## Trademarks

 Eclipse openK User Modules is a trademark of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/openk-usermodules
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.contactBaseData.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.contactBaseData.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.mics.centralService
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.mics.homeService
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.docu
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.frontend

## Third-party Content

This project leverages the following third party content.

ag-grid (18.0.1)

* License: MIT
* Project: https://www.ag-grid.com/
* Source: https://github.com/ag-grid/ag-grid

ag-grid-angular (n/a)

* License: MIT
* Project: https://www.ag-grid.com/
* Source: https://github.com/ag-grid/ag-grid-angular

ag-grid-angular (20.0.0)

* License: MIT
* Project: https://www.ag-grid.com/
* Source: https://github.com/ag-grid/ag-grid-angular

ag-grid-community (20.0.0)

* License: MIT
* Project: https://www.ag-grid.com/
* Source: https://github.com/ag-grid/ag-grid

ajv (6.5.2)

* License: MIT AND (BSD-3-Clause OR AFL-2.1)
* Source: https://github.com/epoberezkin/ajv/releases/tag/v6.5.2

angular animations (5.2.11)

* License: MIT
* Project: https://angular.io/guide/animations
* Source: https://github.com/angular/angular/releases/tag/5.2.11

angular animations (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/guide/animations
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular calendar (0.23.7)

* License: MIT
* Source: https://github.com/mattlewis92/angular-calendar/releases/tag/v0.23.7

angular common (5.2.11)

* License: MIT
* Source: https://github.com/angular/angular/releases/tag/5.2.11

angular common (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/api/common
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular compiler (5.2.11)

* License: MIT
* Source: https://github.com/angular/angular/releases/tag/5.2.11

angular compiler (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular core (5.2.11)

* License: MIT
* Source: https://github.com/angular/angular/releases/tag/5.2.11

angular core (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular font awesome (3.1.2)

* License: MIT

angular forms (5.2.11)

* License: MIT
* Source: https://github.com/angular/angular/releases/tag/5.2.11

angular forms (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular http (5.2.11)

* License: MIT
* Source: https://github.com/angular/angular/releases/tag/5.2.11

angular http (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular jwt (1.0)

* License: MIT
* Source: https://github.com/auth0/angular2-jwt

angular material2 (5.2.5)

* License: MIT
* Source: https://github.com/angular/material2/tree/5.2.5

angular platform-browser (5.2.11)

* License: MIT
* Source: https://github.com/angular/angular/releases/tag/5.2.11

angular platform-browser (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular platform-browser-dynamic (5.2.11)

* License: MIT
* Source: https://github.com/angular/angular/releases/tag/5.2.11

angular platform-browser-dynamic (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular router (5.2.11)

* License: MIT
* Source: https://github.com/angular/angular/releases/tag/5.2.11

angular router (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular seed 2018-11-15 (n/a)

* License: MIT

angular service-worker (9.0.0)

* License: MIT

angular2-uuid (1.1.1)

* License: MIT

Apache Commons Lang (2.6)

* License: Apache License, 2.0

Apache HttpClient (4.5.3)

* License: Apache-2.0

Apache HttpClient (4.5.3)

* License: Apache-2.0

Apache Log4j (1.2.17)

* License: Apache License 2.0

arc42 template (n/a)

* License: MIT
* Project: https://arc42.org/overview/
* Source:
   https://github.com/arc42/arc42-template/raw/master/dist/arc42-template-EN-plain-docx.zip

auth0/angular-jwt (2.0.0)

* License: MIT
* Project: https://github.com/auth0/angular2-jwt
* Source: https://github.com/auth0/angular2-jwt/releases/tag/2.0.0

BIRT Runtime (4.5.0)

* License: EPL-1.0
* Project: https://www.eclipse.org/birt/about/
* Source:
   http://central.maven.org/maven2/org/eclipse/birt/runtime/viewservlets/4.5.0/

Bootstrap (3.3.7)

* License: MIT

bootstrap (4.1.1)

* License: MIT
* Project: https://getbootstrap.com/
* Source: https://github.com/twbs/bootstrap

bootstrap (4.4.1)

* License: MIT

bootstrap toggle (2.2.2)

* License: MIT
* Source: https://github.com/minhur/bootstrap-toggle/releases/tag/2.2.2

camunda-bpm-platform (7.11.0)

* License: Apache-2.0

camunda-engine-dmn (7.11.0)

* License: Apache-2.0

camunda-engine-spring (7.9.0)

* License: Apache-2.0
* Project: https://github.com/camunda/camunda-bpm-platform
* Source: https://github.com/camunda/

cglib (3.1)

* License: Apache License, 2.0, New BSD license

classlist.js (1.1.20150312)

* License: Unlicense

classlist.js (1.1.20150312)

* License: Unlicense

classlist.js (1.1.20150312)

* License: Unlicense

com.google.code.gson : gson : (2.8.5)

* License: Apache-2.0
* Project: https://github.com/google/gson
* Source: https://github.com/google/gson

commons-codec (1.11)

* License: Apache-2.0 AND BSD-3-Clause

commons-codec:commons-codec (1.10)

* License: Apache License, 2.0

commons-io (2.5)

* License: Apache License, 2.0

commons-io:commons-io (2.6)

* License: Apache-2.0

core js (2.5.7)

* License: BSD 3-Clause AND MIT
* Source: https://github.com/zloirock/core-js/releases/tag/v2.5.7

core-js (2.5.4)

* License: MIT
* Project: https://github.com/zloirock/core-js
* Source: https://github.com/zloirock/core-js/releases/tag/v2.5.4

core-js, (2.4.1)

* License: MIT

dozer V.5.5.1 (5.5.1)

* License: Apache License, 2.0

dropwizard (1.3.1)

* License: Apache-2.0 AND MIT AND BSD-3-Clause
* Source: https://github.com/dropwizard/dropwizard/tree/v1.3.1

dropwizard.dropwizard-core (9.0.2)

* License: Apache-2.0

easymock (3.3.1)

* License: Apache License, 2.0

file-saver (1.3.8)

* License: MIT
* Source: https://github.com/eligrey/FileSaver.js/releases/tag/1.3.8

file-saver (2.0.0)

* License: MIT
* Project: https://github.com/eligrey/FileSaver.js#readme
* Source: https://github.com/eligrey/FileSaver.js/releases/tag/2.0.0

font-awesome (4.7.0)

* License: OFL-1.1 AND MIT

font-awesome (4.7.0)

* License: OFL-1.1 AND MIT

font-awesome (4.7.0)

* License: OFL-1.1 AND MIT

gson (2.8.0)

* License: Apache-2.0

h2 Database (1.3.168)

* License: Eclipse Public License

HikariCP-java7 (2.4.13)

* License: Apache-2.0
* Project: http://brettwooldridge.github.io/HikariCP/
* Source: https://github.com/brettwooldridge/HikariCP

jackson-annotations (2.5.4)

* License: Apache-2.0

jackson-core (2.8.6)

* License: Apache-2.0

jackson-databind (2.8.6)


jackson-dataformat-yaml (2.8.6)

* License: Apache-2.0

javax.mail (1.4.3)

* License: (CDDL-1.0 OR GPL-2.0+ WITH Classpath-exception-2.0)

javax.servlet-api (3.1.0)

* License: Apache-2.0 AND (CDDL-1.1 OR GPL-2.0 WITH Classpath-exception-2.0)

javax.servlet-api (3.1.0)

* License: Apache-2.0 AND (CDDL-1.1 OR GPL-2.0 WITH Classpath-exception-2.0)

jawa-jwt (3.2.0)

* License: MIT

jersey-bundles-jaxrs-ri (jaxrs-ri-2.22.1.jar) (2.22.1)

* License: Common Development and Distribution License, Apache 2.0

jersey-container-servlet-core (2.23.2)

* License: Common Development and Distribution License 1.1

jersey-media-json-jackson (2.23.2)

* License: CDDL-1.1 OR GPL-2.0 With Classpath-exception-2.0

jersey-spring3 (2.23.2)

* License: CDDL-1.1 OR GPL-2.0 With Classpath-exception-2.0
* Project:
   https://mvnrepository.com/artifact/org.glassfish.jersey.ext/jersey-spring3
* Source:
   https://repo1.maven.org/maven2/org/glassfish/jersey/ext/jersey-spring3/2.23.2/

jjwt (0.6.0)

* License: Apache License, 2.0

joda-time (2.9.4)

* License: Apache License, 2.0

jQuery (3.3.1)

* License: MIT

jQuery (3.4.1)

* License: MIT

JUnit (4.12)

* License: Eclipse Public License

mapstruct processor (1.2.0)

* License: Apache-2.0 AND CC-BY-2.5 AND LicenseRef-Public-Domain

mockito (1.9.5)

* License: Apache License, 2.0, New BSD license, MIT license

moment (2.24.0)

* License: MIT

ng-bootstrap (2.2.0)

* License: MIT
* Project: https://ng-bootstrap.github.io/#/home
* Source: https://github.com/ng-bootstrap/ng-bootstrap

ng-bootstrap (3.3.0)

* License: MIT AND Apache-2.0
* Project: https://ng-bootstrap.github.io/#/home
* Source: https://github.com/ng-bootstrap/ng-bootstrap

ng2 daterangepicker (2.0.12)

* License: MIT
* Project: https://www.npmjs.com/package/ng2-daterangepicker
* Source:
   https://github.com/evansmwendwa/ng2-daterangepicker/releases/tag/2.0.12

ng2-tree-2.0.0-rc.11 (2.0.0)

* License: MIT
* Source: https://github.com/valor-software/ng2-tree/tree/v2.0.0-rc.11

ngrx-forms (5.2.1)

* License: MIT

ngx-toastr (11.2.1)

* License: MIT

ngx-translate core, (11.0.1)

* License: MIT

ngx-translate http-loader, (4.0.0)

* License: MIT

npm-install-peers (1.2.1)

* License: MIT

org.apache.commons.email (1.4)

* License: Apache-2.0

Popper (1.14.3)

* License: MIT AND CC-BY-3.0 AND (MIT OR GPL-2.0) AND LicenseRef-Public-Domain
* Project: https://popper.js.org/
* Source: https://github.com/FezVrasta/popper.js/releases/tag/v1.14.3

postgres.postgresql (9.1)

* License: BSD-3-Clause AND License-Ref-Public-Domain

PostgreSQL JDBC Driver (42.2.8)

* License: BSD-2-Clause AND BSD-3-Clause AND Apache-2.0

powermock (1.7.3)

* License: Apache-2.0 AND BSD-3-Clause AND MIT AND (GPL-2.0 OR GPL-2.0 With
   Classpath-exception) AND LicenseRef-Public-Domain
* Source: https://github.com/powermock/powermock/tree/powermock-1.7.3

powermock-module-junit4-common (1.6.6)

* License: Apache-2.0

primeicons (1.0.0)

* License: MIT
* Project: https://www.primefaces.org/primeng/#/
* Source: https://github.com/primefaces/primeicons

primeng (6.0.0)

* License: MIT
* Project: https://www.primefaces.org/primeng/#/
* Source: https://github.com/primefaces/primeng

primeng (6.1.3)

* License: MIT

primeng (6.1.3)

* License: MIT
* Project: https://www.primefaces.org/primeng/#/
* Source: https://github.com/primefaces/primeng

RabbitMQ AMQP client (5.2.0)

* License: MPL-2.0 OR GPL-2.0 OR Apache-2.0

reselect (4.0.0)

* License: MIT

rxjs (5.5.11)

* License: Apache-2.0
* Source: https://github.com/ReactiveX/rxjs/releases/tag/5.5.11

rxjs (6.2.2)

* License: Apache-2.0
* Project: https://github.com/Reactive-Extensions/RxJS
* Source: https://github.com/Reactive-Extensions/RxJS

rxjs-compat (6.5.4)

* License: Pending

slf4j-api (1.7.5)

* License: MIT License + MIT License with no endorsement clause

slf4j-log4j12 (1.7.25)

* License: MIT

Source Sans Pro (n/a)

* License: OFL-1.1
* Project:
   http://www.adobe.com/products/type/font-information/source-sans-pro-readme.html

spinKit (1.2.5)

* License: MIT

Spring Boot Data JPA Starter (2.2.1)

* License: Apache-2.0

Spring Boot OAuth2/OpenID Connect Client Starter (2.2.1)

* License: Apache-2.0

Spring Boot Security Starter (2.2.1)

* License: Apache-2.0

Spring Boot Web Starter (2.2.1)

* License: Apache-2.0

Spring Cloud Dependencies Hoxton.RELEASE (n/a)

* License: Apache-2.0

Spring Cloud Starter OpenFeign (2.2.0)

* License: Pending

spring-data-jpa (1.9.1)

* License: Apache License, 2.0

spring-jdbc (4.3.17)

* License: Apache-2.0
* Project: https://spring.io/
* Source:
   http://central.maven.org/maven2/org/springframework/spring-jdbc/4.3.17.RELEASE/

spring-test (4.3.17)

* License: Apache-2.0 AND LicenseRef-Public-Domain
* Project: https://spring.io/
* Source:
   http://central.maven.org/maven2/org/springframework/spring-test/3.2.17.RELEASE/

spring-web (4.3.17)

* License: Apache-2.0
* Project: https://spring.io/
* Source: https://github.com/spring-projects/spring-framework

springfox-swagger2 (2.9.2)

* License: Pending

swagger-core (1.5.12)

* License: Apache-2.0

swagger-jersey2-jaxrs (1.5.12)

* License: Apache-2.0

swimlane/ngx-datatable (15.0.2)

* License: MIT

ts-helpers (1.1.2)

* License: MIT

tslib (1.10.0)

* License: Apache-2.0

urlrewritefilter (4.0.4)

* License: BSD-3-Clause AND (Apache-2.0 OR LGPL-2.0 OR GPL-2.0)
* Project: https://github.com/paultuckey/urlrewritefilter
* Source: https://github.com/paultuckey/urlrewritefilter

web-animations (2.3.1)

* License: Apache-2.0 AND BSD-3-Clause AND MIT
* Source: https://github.com/web-animations/web-animations-js

web-animations-js (2.3.1)

* License: Apache-2.0
* Project: https://github.com/web-animations/web-animations-js
* Source:
   https://github.com/web-animations/web-animations-js/releases/tag/2.3.1

web-animations-js (2.3.2)

* License: Pending

zone.js (0.8.26)

* License: MIT

zone.js (0.8.26)

* License: MIT
* Source: https://github.com/angular/zone.js/releases/tag/v0.8.26

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
