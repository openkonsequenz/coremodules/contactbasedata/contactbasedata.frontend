/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { Component, OnInit, HostBinding } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppSandbox } from './app.sandbox';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '@shared/models/user';

/**
 * Main Component
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @class AppComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  providers: [AppSandbox],
})
export class AppComponent implements OnInit {
  @HostBinding('class.body-loginPage')
  public isLoginPage: boolean;

  /**
   * Creates an instance of AppComponent.
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @param {Router} router
   * @param {AppSandbox} appSandbox
   * @param {ActivatedRoute} activatedRoute
   * @memberof AppComponent
   */
  constructor(public router: Router, public appSandbox: AppSandbox, private activatedRoute: ActivatedRoute) {}

  /**
   * Initiates the component
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @memberof AppComponent
   */
  ngOnInit() {
    this.appSandbox.setupLanguage();
    this._extractTokenFromParameters();
    this._registerEvents();
  }

  /**
   * Processes the acces token given via URL
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @private
   * @param {string} accessToken
   * @memberof AppComponent
   */
  private _processAccessToken(accessToken: string) {
    const jwtHelper: JwtHelperService = new JwtHelperService();
    const decoded: any = jwtHelper.decodeToken(accessToken);
    const user: User = new User();

    const firstName = decoded.given_name || '';
    const lastName = decoded.family_name || '';

    user.id = decoded.sub;
    user.username = decoded.preferred_username;
    user.itemName = decoded.preferred_username;
    user.name = firstName + ' ' + lastName;
    user.roles = [];

    //add realm roles to jwtPayload.roles
    if (decoded.realm_access && decoded.realm_access.roles) {
      user.roles = decoded.realm_access.roles;
    }

    const clientRoles = [];
    const resourceAccessList = decoded.resource_access;
    for (const el in resourceAccessList) {
      clientRoles.push(...resourceAccessList[el].roles);
    }
    user.roles.push(...clientRoles);

    localStorage.setItem('token', accessToken);
    this.appSandbox.setUser(user);
  }

  /**
   * Extract the params (suscribe to router event) and store them in the sessionContext.
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @private
   * @memberof AppComponent
   */
  private _extractTokenFromParameters() {
    this.activatedRoute.params.subscribe(params => {
      const accessToken = this._getParametersFromUrl('accessToken');
      if (!!accessToken) {
        this._processAccessToken(accessToken);
      } else {
        this.appSandbox.setUser();
      }
    });
  }

  /**
   * Sets all required paramter extracted from URL
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @private
   * @param {*} findParam
   * @returns
   * @memberof AppComponent
   */
  private _getParametersFromUrl(findParam) {
    const parameterUrl = window.location.search.substr(1);
    return parameterUrl != null && parameterUrl !== '' ? this._readParamAccessToken(parameterUrl, findParam) : null;
  }

  /**
   * Reads parameter from access token
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @private
   * @param {*} prmstr
   * @param {*} findParam
   * @returns
   * @memberof AppComponent
   */
  private _readParamAccessToken(prmstr, findParam) {
    const params = {};
    const prmarr = prmstr.split('&');
    for (let i = 0; i < prmarr.length; i++) {
      const tmparr = prmarr[i].split('=');
      params[tmparr[0]] = tmparr[1];
    }
    return params[findParam];
  }

  /**
   * Registers events needed for the application
   * Subscribes to route change event and sets "isLoginPage" variable
   * in order to set correct CSS class on body tag.
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @private
   * @memberof AppComponent
   */
  private _registerEvents(): void {
    this.router.events.subscribe(route => {
      this.isLoginPage = route['url'] === '/overview';
    });
  }
}
