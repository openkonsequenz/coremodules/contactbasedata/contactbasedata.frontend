/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { LogoutPageComponent } from '@pages/logout/logout/logout.component';

describe('LogoutPageComponent', () => {
  let component: LogoutPageComponent;
  let logoutPageSandBox: any;

  beforeEach(() => {
    logoutPageSandBox = {
      endSubscriptions() {},
    } as any;

    component = new LogoutPageComponent(logoutPageSandBox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should end all subscriptions for this sandbox onDestroy', () => {
    const spy = spyOn(logoutPageSandBox, 'endSubscriptions');
    component.ngOnDestroy();

    expect(spy).toHaveBeenCalled();
  });
});
