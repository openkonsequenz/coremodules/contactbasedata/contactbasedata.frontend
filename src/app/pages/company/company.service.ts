/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Company, CommunicationsData, ContactPerson, Address } from '@shared/models';
import { Injectable } from '@angular/core';

/**
 * Used to retrieve company
 *
 * @export
 * @class CompanyService
 */
@Injectable()
export class CompanyService {
  /**
   * Transforms company details recieved from the API into instance of 'Company'
   *
   * @param company
   */
  static companyDetailsAdapter(company: any): Company {
    return new Company(company);
  }

  /**
   * Transforms communications data received from the API into instance of 'CommunicationsData[]'
   *
   * @param communicationsData
   */
  static communicationsDataAdapter(communicationsData: any): Array<CommunicationsData> {
    return communicationsData.map(communicationsData => new CommunicationsData(communicationsData));
  }

  /**
   * Transforms communications data details received from the API into instance of 'CommunicationsData'
   *
   * @param response
   */
  static communicationsDataDetailsAdapter(communicationsData: CommunicationsData): CommunicationsData {
    return new CommunicationsData(communicationsData);
  }

  /**
   * Transforms contact persons received from the API into instance of 'ContactPersons[]'
   *
   * @param contactPersons
   */
  static contactPersonsAdapter(response: any): Array<ContactPerson> {
    return response.map(item => new ContactPerson(item));
  }

  /**
   * Transforms company's details addresses received from the API into instance of 'address[]'
   *
   * @param addresses
   */
  static addressesAdapter(addresses: any): Array<Address> {
    return addresses.map(address => new Address(address));
  }

  /**
   * Transforms company's details addresses details recieved from the API into instance of 'address'
   *
   * @param response
   */
  static addressesDetailsAdapter(address: any): Address {
    return new Address(address);
  }

  /**
   * Transforms company's contact person details recieved from the API into instance of 'ContactPerson'
   *
   * @param response
   */
  static contactPersonDetailsAdapter(response: any): ContactPerson {
    return new ContactPerson(response);
  }
}
