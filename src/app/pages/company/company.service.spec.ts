import { ContactPerson } from '@shared/models';
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CompanyService } from '@pages/company/company.service';
import { CommunicationsData, Address } from '@shared/models';

describe('CompanyService', () => {
  beforeEach(() => {});

  it('should transform company details', () => {
    const company: any = { companyName: 'Heysterkamp' };
    const transContact = CompanyService.companyDetailsAdapter(company);

    expect(transContact.companyName).toBe(company.companyName);
  });

  it('should transform communication data details', () => {
    const communicationsData: any = { communicationTypeId: 'Heysterkamp' };
    const transContact = CompanyService.communicationsDataDetailsAdapter(communicationsData);

    expect(transContact.communicationTypeId).toBe(communicationsData.communicationTypeId);
  });

  it('should transform communication data list', () => {
    const response = [new CommunicationsData()];
    response[0].communicationTypeId = 'Herr';

    expect(CompanyService.communicationsDataAdapter(response)[0].communicationTypeId).toBe('Herr');
  });

  it('should transform address data details', () => {
    const addressData: any = { addressTypeId: 'Heysterkamp' };
    const transContact = CompanyService.addressesDetailsAdapter(addressData);

    expect(transContact.addressTypeId).toBe(addressData.addressTypeId);
  });

  it('should transform address data list', () => {
    const response = [new Address()];
    response[0].addressTypeId = 'Herr';

    expect(CompanyService.addressesAdapter(response)[0].addressTypeId).toBe('Herr');
  });

  it('should transform contact person list', () => {
    const response = [new ContactPerson()];
    response[0].firstName = 'Tabea';

    expect(CompanyService.contactPersonsAdapter(response)[0].firstName).toBe('Tabea');
  });

  it('should transform contact person details', () => {
    const response: any = { companyName: 'Test GmbH' };
    const contact = CompanyService.contactPersonDetailsAdapter(response);

    expect(contact.companyName).toBe(response.companyName);
  });
});
