/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CompanyDetailsComponent } from '@pages/company/company-details/company-details.component';

describe('CompanyDetailsComponent', () => {
  let component: CompanyDetailsComponent;
  let companyDetailsSandBox: any;
  let userModuleAssignmentSandBox: any;
  let translate: any;

  beforeEach(() => {
    companyDetailsSandBox = {
      registerAddressEvents() {},
      registerCompanyEvents() {},
      endSubscriptions() {},
      loadCompanyDetailsAddressDetails() {},
      updatingState() {},
      currentFormState: { isValid: true },
      persistCompany() {},
      loadCommunicationsDataDetails() {},
      utilService: { displayNotification: () => {} },
    } as any;

    translate = {
      instant() {},
    } as any;

    component = new CompanyDetailsComponent(companyDetailsSandBox, userModuleAssignmentSandBox, translate);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call translate instant if no communications data id', () => {
    const spy = spyOn(translate, 'instant');
    component.loadCommunicationsDataDetail(null);

    expect(spy).toHaveBeenCalledWith('CommunicationsData.NewCommunicationsData');
  });

  it('should call translate instant if there is an communications data id', () => {
    const id = 'id';
    const spy = spyOn(translate, 'instant');
    const spy1 = spyOn(companyDetailsSandBox, 'loadCommunicationsDataDetails');
    component.loadCommunicationsDataDetail(id);

    expect(spy).toHaveBeenCalledWith('CommunicationsData.EditCommunicationsData');
    expect(spy1).toHaveBeenCalledWith(id);
  });

  it('should call translate instant if there is an communications data id', () => {
    component.companyDetailsSandBox.isCommunicationsDataDetailViewVisible = false;
    component.loadCommunicationsDataDetail(null);
    expect(component.companyDetailsSandBox.isCommunicationsDataDetailViewVisible).toBeTruthy();
  });

  it('should loadCompanies on init', () => {
    const spy = spyOn(companyDetailsSandBox, 'registerCompanyEvents');
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should end all subscriptions for this sandbox onDestroy', () => {
    const spy = spyOn(companyDetailsSandBox, 'endSubscriptions');
    component.ngOnDestroy();

    expect(spy).toHaveBeenCalled();
  });

  it('should call translate instant if no id', () => {
    const spy = spyOn(translate, 'instant');
    component.loadAddressDetail(null);

    expect(spy).toHaveBeenCalledWith('Address.NewAddress');
  });

  it('should call translate instant if there is id', () => {
    const id = 'id';
    const spy = spyOn(translate, 'instant');
    const spy1 = spyOn(companyDetailsSandBox, 'loadCompanyDetailsAddressDetails');
    component.loadAddressDetail(id);

    expect(spy).toHaveBeenCalledWith('Address.EditAddress');
    expect(spy1).toHaveBeenCalledWith(id);
  });

  it('should call translate instant if there is id', () => {
    component.companyDetailsSandBox.isAddressDataDetailViewVisible = false;
    component.loadAddressDetail(null);
    expect(component.companyDetailsSandBox.isAddressDataDetailViewVisible).toBeTruthy();
  });

  it('should set expandableVisible to "true" if companyContactId is defined', () => {
    component.companyDetailsSandBox.companyContactId = 'test';
    (component as any)._initExpandableState();
    expect(component.isExpandableVisible).toBeTruthy();
    expect(component.companyDetailsSandBox.isAddressDataDetailViewVisible).toBeFalsy();
    expect(component.companyDetailsSandBox.isCommunicationsDataDetailViewVisible).toBeFalsy();
  });
});
