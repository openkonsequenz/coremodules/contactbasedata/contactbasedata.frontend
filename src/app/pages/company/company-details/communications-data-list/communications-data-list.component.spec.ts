/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { CompanyCommunicationsDataListComponent } from '@pages/company/company-details/communications-data-list/communications-data-list.component';
import { of } from 'rxjs';

describe('CompanyCommunicationsDataListComponent', () => {
  let component: CompanyCommunicationsDataListComponent;
  let companySandbox: any;

  beforeEach(async(() => {
    companySandbox = {
      registerCommunicationsDataEvents() {},
      registerCompanyEvents() {},
      endSubscriptions() {},
      deleteCommunicationsData() {},
      newCommunicationsData() {},
    } as any;
    companySandbox.communicationsAgApi = { setDomLayout() {} };
  }));

  beforeEach(() => {
    component = new CompanyCommunicationsDataListComponent(companySandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define gridOptions onInit', () => {
    component.ngOnInit();

    expect(component.gridOptions).toBeDefined();
    expect(component.gridOptions.context).toBeDefined();
  });

  it('should emit if BusEvents is edit ', () => {
    const spy = spyOn(component.companyDetailsIdLoaded, 'emit');
    const event: any = { type: 'edit', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should deleteCommunicationsData if BusEvents is delete', () => {
    const spy = spyOn(companySandbox, 'deleteCommunicationsData');
    const event: any = { type: 'delete', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should call newCommunicationsData and close edit area if callcreateNewCommunicationsDataForm', () => {
    const spy = spyOn(companySandbox, 'newCommunicationsData');
    const spyEmit = spyOn(component.createNewCompany, 'emit');
    component.createNewCommunicationsDataForm();

    expect(spy).toHaveBeenCalled();
    expect(spyEmit).toHaveBeenCalled();
  });
});
