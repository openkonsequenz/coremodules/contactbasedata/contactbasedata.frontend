/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CompanyDetailsSandBox } from '@pages/company/company-details/company-details.sandbox';
import { Component, OnInit, Input } from '@angular/core';
import { CommunicationTypesSandbox } from '@pages/admin/communication-types/communication-types.sandbox';

@Component({
  selector: 'app-company-communications-data-details',
  templateUrl: './communications-data-details.component.html',
  styleUrls: ['./communications-data-details.component.scss']
})
export class CompanyCommunicationsDataDetailsComponent implements OnInit {

  @Input() createOrEditCommunicationsData: string;

  constructor(
    public companyDetailsSandbox: CompanyDetailsSandBox,
    public communicationTypesSandbox: CommunicationTypesSandbox
  ) { }

  ngOnInit() {
    this.companyDetailsSandbox.registerCommunicationsDataEvents();
    this.companyDetailsSandbox.clearCommunicationsData();
  }
}
