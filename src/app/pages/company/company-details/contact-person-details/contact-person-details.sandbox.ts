/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ofType } from '@ngrx/effects';
import { ActionsSubject, Store } from '@ngrx/store';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { Globals } from '@shared/constants/globals';
import { Address, CommunicationType, CommunicationsData, ContactPerson } from '@shared/models';
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import * as store from '@shared/store';
import * as communicationTypesActions from '@shared/store/actions/admin/communication-types.action';
import * as contactPersonActions from '@shared/store/actions/company/contact-person.actions';
import * as contactPersonAddressDetailsFormReducer from '@shared/store/reducers/company/contact-person-details/addresses-details-form.reducer';
import * as contactPersonCommunicationDataDetailsFormReducer from '@shared/store/reducers/company/contact-person-details/communications-data-details-form.reducer';
import * as contactPersonFormReducer from '@shared/store/reducers/company/contact-person-details/contact-persons-details-form.reducer';
import { UtilService } from '@shared/utility/utility.service';
import { FormGroupState, MarkAsTouchedAction, ResetAction, SetValueAction } from 'ngrx-forms';
import { Observable } from 'rxjs';
import { map, take, takeUntil } from 'rxjs/operators';

@Injectable()
export class ContactPersonDetailsSandbox extends BaseSandbox {
  public formState$ = this.appState$.select(store.getCompanyContactPersonDetails);
  public currentFormState: FormGroupState<ContactPerson>;
  public addressList$: Observable<Array<Address>> = this.appState$.select(store.getContactPersonAddressesData);
  public addressListLoading$: Observable<boolean> = this.appState$.select(store.getContactPersonAddressesLoading);
  public addressDetailsFormState$: Observable<FormGroupState<Address>> = this.appState$.select(store.getContactPersonAddressesDetails);
  public addressDetailsCurrentFormState: FormGroupState<Address>;
  public communicationsDataList$: Observable<Array<CommunicationsData>> = this.appState$.select(store.getContactPersonCommunicationsDataData);
  public communicationsDataListLoading$: Observable<boolean> = this.appState$.select(store.getContactPersonCommunicationsDataLoading);
  public communicationsDataDetailsFormState$: Observable<FormGroupState<CommunicationsData>> = this.appState$.select(
    store.getContactPersonCommunicationsDataDetails
  );
  public communicationsDataDetailsCurrentFormState: FormGroupState<CommunicationsData>;
  public isCommunicationsDataDetailViewVisible: boolean = false;
  public isAddressDataDetailViewVisible: boolean = false;
  public existMainAddress = false;
  public isCurrentAddressMainAddress = false;

  public companyContactId: string;
  public contactPersonContactId: string;

  public communicationsAgApi;
  public addressAgApi;

  private _communicationTypes: Array<CommunicationType> = new Array<CommunicationType>();

  constructor(
    protected appState$: Store<store.State>,
    protected actionsSubject: ActionsSubject,
    protected router: Router,
    protected utilService: UtilService,
    protected modalService: NgbModal
  ) {
    super(appState$);
  }

  public navigateBackToCompany() {
    this.router.navigate([`/${Globals.PATH.COMPANY}/${this.companyContactId}`]);
  }

  public loadContactPersonDetails(id: string): void {
    this.appState$.dispatch(contactPersonActions.loadContactPersonDetail({ payload: id }));
  }

  public newContactPerson(companyContactId: string): void {
    this.appState$.dispatch(new SetValueAction(contactPersonFormReducer.FORM_ID, new ContactPerson({ companyContactId: companyContactId })));
    this.appState$.dispatch(new ResetAction(contactPersonFormReducer.FORM_ID));
  }

  public clearContactPerson(): void {
    this.appState$.dispatch(new SetValueAction(contactPersonFormReducer.FORM_ID, contactPersonFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(contactPersonFormReducer.FORM_ID));
  }

  public saveContactPersonDetails(): void {
    if (this.currentFormState.isValid) {
      this.appState$.dispatch(
        contactPersonActions.saveContactPersonDetail({
          payload: new ContactPerson(this.currentFormState.value),
        })
      );
      this.actionsSubject.pipe(ofType(contactPersonActions.saveContactPersonDetailSuccess), takeUntil(this._endSubscriptions$)).subscribe(() => {
        this.clearContactPerson();
        this.navigateBackToCompany();
      });
    } else {
      this.utilService.displayNotification('MandatoryFieldError', 'alert');
    }
  }

  /* addresses */

  public loadContactPersonAddresses(contactId: string): void {
    this.contactPersonContactId = contactId;
    this.appState$.dispatch(contactPersonActions.loadContactPersonDetailAddresses({ payload: contactId }));
  }

  public loadContactPersonDetailsAddressDetails(contactId: string, addressId: string): void {
    this.appState$.dispatch(contactPersonActions.loadContactPersonDetailAddressDetails({ payload_contactId: contactId, payload_addressId: addressId }));
  }

  public persistAddress(): void {
    if (this.addressDetailsCurrentFormState.isValid) {
      const newAddress = new Address(this.addressDetailsCurrentFormState.value);
      newAddress.contactId = newAddress.contactId !== null ? newAddress.contactId : this.contactPersonContactId;
      this.appState$.dispatch(contactPersonActions.persistAddressDetail({ payload: newAddress }));
      this.actionsSubject.pipe(ofType(contactPersonActions.persistAddressDetailSuccess), take(1), takeUntil(this._endSubscriptions$)).subscribe(() => {
        this.closeAddressDataDetail();
        this.isAddressDataDetailViewVisible = false;
      });
    } else {
      this.utilService.displayNotification('MandatoryFieldsNotFilled', 'alert');
    }
  }

  public deleteAddress(address: Address): void {
    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.appState$.dispatch(contactPersonActions.deleteAddress({ payload: address }));
        if (address.isMainAddress) {
          this.existMainAddress = false;
        }
        this.closeAddressDataDetail();
      },
      () => {}
    );
  }

  public closeAddressDataDetail(): void {
    this.clearAddressData();
    this.isAddressDataDetailViewVisible = false;
    this.addressAgApi.setDomLayout('autoHeight');
  }

  public newAddressData(): void {
    this.clearAddressData();
    this.appState$.dispatch(new MarkAsTouchedAction(contactPersonAddressDetailsFormReducer.FORM_ID));
  }

  public clearAddressData(): void {
    this.isCurrentAddressMainAddress = false;
    this.appState$.dispatch(new SetValueAction(contactPersonAddressDetailsFormReducer.FORM_ID, contactPersonAddressDetailsFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(contactPersonAddressDetailsFormReducer.FORM_ID));
  }

  /* communications data */

  public loadContactPersonCommunicationsData(contactId: string): void {
    this._getCommuncationTypes().subscribe((payload: Array<CommunicationType>) => {
      this._communicationTypes = payload;
      this.appState$.dispatch(contactPersonActions.loadContactPersonDetailCommunicationsData({ payload: contactId }));
    });

    this._getCommunicationsData().subscribe((communicationsData: Array<CommunicationsData>) => {
      if (this._communicationTypes) {
        this._communicationTypes = this._communicationTypes.map(ct => {
          const existingCommunicationsData: CommunicationsData = communicationsData.find(cd => cd.communicationTypeId == ct.id);
          ct = { ...ct, isDisabled: !!existingCommunicationsData };
          return ct;
        });
        this.appState$.dispatch(communicationTypesActions.loadCommunicationTypesSuccess({ payload: this._communicationTypes }));
      }
    });
  }

  private _getCommunicationsData() {
    return this.actionsSubject.pipe(
      ofType(contactPersonActions.loadContactPersonDetailCommunicationsDataSuccess),
      map((action: contactPersonActions.ILoadContactPersonCommunicationsDataSuccess) => action.payload),
      take(1),
      takeUntil(this._endSubscriptions$)
    );
  }

  private _getCommuncationTypes(): Observable<CommunicationType[]> {
    return this.actionsSubject.pipe(
      ofType(communicationTypesActions.loadCommunicationTypesSuccess),
      map((action: communicationTypesActions.ILoadCommunicationTypesSuccess) => action.payload),
      take(1),
      takeUntil(this._endSubscriptions$)
    );
  }

  public loadCommunicationsDataDetails(contactId: string, communicationsDataId: string): void {
    this.appState$.dispatch(
      contactPersonActions.loadContactPersonDetailCommunicationsDataDetails({ payload_contactId: contactId, payload_communicationsId: communicationsDataId })
    );
  }

  public persistCommunicationsData(): void {
    if (this.communicationsDataDetailsCurrentFormState.isValid) {
      const newCommunicationsData = new CommunicationsData(this.communicationsDataDetailsCurrentFormState.value);
      newCommunicationsData.contactId = newCommunicationsData.contactId !== null ? newCommunicationsData.contactId : this.contactPersonContactId;
      this.appState$.dispatch(contactPersonActions.persistCommunicationsDataDetail({ payload: newCommunicationsData }));
      this.actionsSubject
        .pipe(ofType(contactPersonActions.persistCommunicationsDataDetailSuccess), take(1), takeUntil(this._endSubscriptions$))
        .subscribe(() => {
          this.closeCommunicationsDataDetail();
        });
    } else {
      this.utilService.displayNotification('MandatoryFieldsNotFilled', 'alert');
    }
  }

  public deleteCommunicationsData(communicationsData: CommunicationsData): void {
    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.appState$.dispatch(contactPersonActions.deleteCommunicationsData({ payload: communicationsData }));
        this.closeCommunicationsDataDetail();
      },
      () => {}
    );
  }

  public closeCommunicationsDataDetail(): void {
    // zum ausgrauen der bereits verwendeten comm.kanäle
    this.loadContactPersonCommunicationsData(this.companyContactId);
    this.clearCommunicationsData();
    this.isCommunicationsDataDetailViewVisible = false;
    this.communicationsAgApi.setDomLayout('autoHeight');
  }

  public newCommunicationsData(): void {
    this.clearCommunicationsData();
    this.appState$.dispatch(new MarkAsTouchedAction(contactPersonCommunicationDataDetailsFormReducer.FORM_ID));
  }

  public clearCommunicationsData(): void {
    this.appState$.dispatch(
      new SetValueAction(contactPersonCommunicationDataDetailsFormReducer.FORM_ID, contactPersonCommunicationDataDetailsFormReducer.INITIAL_STATE.value)
    );
    this.appState$.dispatch(new ResetAction(contactPersonCommunicationDataDetailsFormReducer.FORM_ID));
  }

  /* register events */

  public registerEvents(): void {
    this.formState$.pipe(takeUntil(this._endSubscriptions$)).subscribe((formState: FormGroupState<ContactPerson>) => (this.currentFormState = formState));
  }

  public registerCommunicationsDataEvents(): void {
    // subscribes to formState
    this.communicationsDataDetailsFormState$
      .pipe(takeUntil(this._endSubscriptions$))
      .subscribe((formState: FormGroupState<CommunicationsData>) => (this.communicationsDataDetailsCurrentFormState = formState));
  }

  public registerAddressEvents(): void {
    // subscribes to formState
    this.addressDetailsFormState$
      .pipe(takeUntil(this._endSubscriptions$))
      .subscribe((formState: FormGroupState<Address>) => (this.addressDetailsCurrentFormState = formState));
  }
}
