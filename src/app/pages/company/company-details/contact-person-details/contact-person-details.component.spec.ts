/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CompanyContactPersonDetailsComponent } from './contact-person-details.component';
import { CommunicationsData, Address } from '@app/shared/models';

describe('ContactPersonDetailsComponent', () => {
  let component: CompanyContactPersonDetailsComponent;
  let contactPersonDetailSandbox: any;
  let salutationSandbox: any;
  let personTypesSandbox: any;
  let userModuleAssignmentSandBox: any;
  let translate: any;

  beforeEach(() => {
    contactPersonDetailSandbox = {
      registerEvents() {},
      registerAddressEvents() {},
      registerCompanyEvents() {},
      endSubscriptions() {},
      loadContactPersonDetailsAddressDetails() {},
      updatingState() {},
      currentFormState: { isValid: true },
      persistCompany() {},
      loadCommunicationsDataDetails() {},
      utilService: { displayNotification: () => {} },
    } as any;
    salutationSandbox = {} as any;
    personTypesSandbox = {} as any;
    userModuleAssignmentSandBox = {} as any;
    translate = {
      instant() {},
    } as any;

    component = new CompanyContactPersonDetailsComponent(
      contactPersonDetailSandbox,
      salutationSandbox,
      personTypesSandbox,
      userModuleAssignmentSandBox,
      translate
    );
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should call translate instant if no communications data id', () => {
    const spy = spyOn(translate, 'instant');
    component.loadCommunicationsDataDetail(null);

    expect(spy).toHaveBeenCalledWith('CommunicationsData.NewCommunicationsData');
  });

  it('should call translate instant if there is an communications data id', () => {
    const comm = new CommunicationsData();
    const spy = spyOn(translate, 'instant');
    const spy1 = spyOn(contactPersonDetailSandbox, 'loadCommunicationsDataDetails');
    component.loadCommunicationsDataDetail(comm);

    expect(spy).toHaveBeenCalledWith('CommunicationsData.EditCommunicationsData');
    expect(spy1).toHaveBeenCalledWith(comm.contactId, comm.id);
  });

  it('should loadCompanies on init', () => {
    const spy = spyOn(contactPersonDetailSandbox, 'registerEvents');
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should end all subscriptions for this sandbox onDestroy', () => {
    const spy = spyOn(contactPersonDetailSandbox, 'endSubscriptions');
    component.ngOnDestroy();

    expect(spy).toHaveBeenCalled();
  });

  it('should call translate instant if loadAddressDetail without id', () => {
    const spy = spyOn(translate, 'instant');
    component.loadAddressDetail(null);

    expect(spy).toHaveBeenCalledWith('Address.NewAddress');
  });

  it('should call translate instant if there is id', () => {
    const address = new Address();
    const spy = spyOn(translate, 'instant');
    const spy1 = spyOn(contactPersonDetailSandbox, 'loadContactPersonDetailsAddressDetails');
    component.loadAddressDetail(address);

    expect(spy).toHaveBeenCalledWith('Address.EditAddress');
    expect(spy1).toHaveBeenCalledWith(address.contactId, address.id);
  });
});
