import { loadContactPersonDetail } from './../../../../shared/store/actions/company/contact-person.actions';
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ContactPersonDetailsSandbox } from '@pages/company/company-details/contact-person-details/contact-person-details.sandbox';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { SalutationsSandbox } from '@pages/admin/salutations/salutations.sandbox';
import { PersonTypesSandbox } from '@pages/admin/person-types/person-types.sandbox';
import { CommunicationTypesSandbox } from '@pages/admin/communication-types/communication-types.sandbox';
import { AddressTypesSandbox } from '@pages/admin/address-types/address-types.sandbox';
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';

@Injectable()
export class ContactPersonDetailsResolver  {
  constructor(
    private _contactPersonDetailsSandbox: ContactPersonDetailsSandbox,
    private _salutationsSandbox: SalutationsSandbox,
    private _personTypesSandbox: PersonTypesSandbox,
    private _communicationTypesSandbox: CommunicationTypesSandbox,
    private _addressTypesSandbox: AddressTypesSandbox,
    private _userModuleAssignmentSandbox: UserModuleAssignmentSandBox
  ) {}

  public resolve(route: ActivatedRouteSnapshot): void {
    const companyContactId: string = route.params['contactId'];
    if (companyContactId) {
      this._contactPersonDetailsSandbox.companyContactId = companyContactId;
    }
    const contactPersonId: string = route.params['contactPersonId'];
    if (contactPersonId && contactPersonId !== 'new') {
      this._contactPersonDetailsSandbox.loadContactPersonDetails(contactPersonId);
      this._contactPersonDetailsSandbox.loadContactPersonAddresses(contactPersonId);
      this._contactPersonDetailsSandbox.loadContactPersonCommunicationsData(contactPersonId);
      this._userModuleAssignmentSandbox.loadUserModuleAssignments(contactPersonId);
    } else {
      this._contactPersonDetailsSandbox.newContactPerson(companyContactId);
    }
    this._salutationsSandbox.loadSalutations();
    this._personTypesSandbox.loadPersonTypes();
    this._communicationTypesSandbox.loadCommunicationTypes();
    this._addressTypesSandbox.loadAddressTypes();
  }
}
