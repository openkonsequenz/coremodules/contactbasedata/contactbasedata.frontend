/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ContactPersonDetailsSandbox } from '@pages/company/company-details/contact-person-details/contact-person-details.sandbox';
import { Component, OnInit, Input } from '@angular/core';
import { AddressTypesSandbox } from '@pages/admin/address-types/address-types.sandbox';

@Component({
  selector: 'app-contact-person-address-details',
  templateUrl: './address-details.component.html',
  styleUrls: ['./address-details.component.scss'],
})
export class ContactPersonAddressDetailsComponent implements OnInit {
  @Input() createOrEditAddressData: string;

  constructor(public contactPersonDetailsSandbox: ContactPersonDetailsSandbox, public addressTypesSandbox: AddressTypesSandbox) {}

  ngOnInit() {
    this.contactPersonDetailsSandbox.registerAddressEvents();
    this.contactPersonDetailsSandbox.clearAddressData();
  }
}
