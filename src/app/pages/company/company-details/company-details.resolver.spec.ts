/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { CompanyDetailsResolver } from '@pages/company/company-details/company-details.resolver';

describe('CompanyDetailsResolver', () => {
  let component: CompanyDetailsResolver;
  let companyDetailsSandbox: any;
  let userModuleAssignmentSandbox: any;
  let communicationTypesSandbox: any;
  let addressTypesSandbox: any;

  beforeEach(async(() => {
    companyDetailsSandbox = {
      clearCompany() {},
      loadCompany() {},
      loadCommunicationsData() {},
      loadCompanyAddresses() {},
      loadContactPersons() {},
    } as any;

    communicationTypesSandbox = {
      loadCommunicationTypes() {},
    } as any;

    userModuleAssignmentSandbox = {
      loadFilteredUserModuleTypes() {},
      loadUserModuleAssignments() {},
    } as any;

    addressTypesSandbox = {
      loadAddressTypes() {},
    } as any;
  }));

  beforeEach(() => {
    component = new CompanyDetailsResolver(companyDetailsSandbox, userModuleAssignmentSandbox, communicationTypesSandbox, addressTypesSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call loadCompany if edit a company', () => {
    const spy = spyOn(companyDetailsSandbox, 'loadCompany');
    let ar: any = { params: { contactId: 'ID' } };
    component.resolve(ar);
    expect(spy).toHaveBeenCalled();
  });

  it('should call clear if create a new company', () => {
    const spy = spyOn(companyDetailsSandbox, 'clearCompany');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);
    expect(spy).toHaveBeenCalled();
  });

  it('should loadCommunicationTypes on resolve', () => {
    const spy = spyOn(communicationTypesSandbox, 'loadCommunicationTypes');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });

  it('should set isCommunicationsDataDetailViewVisible true on resolve', () => {
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(companyDetailsSandbox.isCommunicationsDataDetailViewVisible).toBeFalsy();
  });

  it('should set isDetailViewVisible true on resolve', () => {
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(companyDetailsSandbox.isDetailViewVisible).toBeFalsy();
  });

  it('should loadAddressTypes on resolve', () => {
    const spy = spyOn(addressTypesSandbox, 'loadAddressTypes');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });
});
