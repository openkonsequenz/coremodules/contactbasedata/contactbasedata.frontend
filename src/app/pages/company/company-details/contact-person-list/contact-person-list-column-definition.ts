/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { IconCellRendererComponent } from '@shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';

export const CONTACT_PERSON_LIST_COLDEF = [
  {
    field: 'lastName',
    headerName: 'ContactPerson.LastName',
    sortable: true,
    flex: 3,
  },
  {
    field: 'firstName',
    headerName: 'ContactPerson.FirstName',
    sortable: true,
    flex: 3,
  },
  {
    field: 'salutationType',
    headerName: 'ContactPerson.Salutation',
    sortable: true,
    flex: 1,
  },
  {
    field: 'title',
    headerName: 'ContactPerson.PersonTitle',
    sortable: true,
    flex: 1,
  },
  {
    field: 'email',
    headerName: 'ContactPerson.EMail',
    sortable: true,
    flex: 3,
  },
  {
    field: 'personType',
    headerName: 'ContactPerson.PersonType',
    sortable: true,
    flex: 2,
  },
  {
    field: 'contactNote',
    headerName: 'Contacts.Note',
    sortable: true,
    flex: 3,
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    minWidth: 110,
    maxWidth: 110,
    lockPosition: true,
    sortable: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRenderer: IconCellRendererComponent,
  },
];
