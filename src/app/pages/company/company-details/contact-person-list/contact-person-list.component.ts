/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BaseList } from '@shared/components/base-components/base.list';
import { CompanyDetailsSandBox } from '@pages/company/company-details/company-details.sandbox';
import { CONTACT_PERSON_LIST_COLDEF } from '@pages/company/company-details/contact-person-list/contact-person-list-column-definition';
import { Router } from '@angular/router';
import { Globals } from '@shared/constants/globals';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-company-contact-person-list',
  templateUrl: './contact-person-list.component.html',
  styleUrls: ['./contact-person-list.component.scss'],
})
export class CompanyContactPersonListComponent extends BaseList implements OnInit, OnDestroy {
  public columnDefinition = CONTACT_PERSON_LIST_COLDEF;
  private _subscription: Subscription;

  constructor(public companyDetailsSandBox: CompanyDetailsSandBox, private _router: Router) {
    super();
  }

  ngOnInit() {
    this.gridOptions.context = {
      ...this.gridOptions.context,
      icons: { edit: true, delete: true },
    };
    this._subscription = this.gridOptions.context.eventSubject.subscribe(event => {
      if (event.type === 'edit' || event.type === 'readonly') {
        this._router.navigate([`${Globals.PATH.COMPANY}/${event.data.companyContactId}/${Globals.PATH.CONTACT_PERSON}/${event.data.contactId}`]);
      }
      if (event.type === 'delete') {
        this.companyDetailsSandBox.deleteContactPerson(event.data);
      }
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
