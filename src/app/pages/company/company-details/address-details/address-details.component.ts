/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CompanyDetailsSandBox } from '@pages/company/company-details/company-details.sandbox';
import { Component, OnInit, Input } from '@angular/core';
import { AddressTypesSandbox } from '@pages/admin/address-types/address-types.sandbox';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionsSubject } from '@ngrx/store';
import * as companyActions from '@shared/store/actions/company/company.action';
import { ofType } from '@ngrx/effects';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-company-address-details',
  templateUrl: './address-details.component.html',
  styleUrls: ['./address-details.component.scss'],
})
export class CompanyAddressDetailsComponent implements OnInit {
  @Input() public createOrEditAddressData: string;

  constructor(
    public companyDetailsSandbox: CompanyDetailsSandBox,
    public addressTypesSandbox: AddressTypesSandbox,
    private modalService: NgbModal,
    private actionsSubject: ActionsSubject
  ) {}

  ngOnInit() {
    this.companyDetailsSandbox.registerAddressEvents();
    this.companyDetailsSandbox.clearAddressData();
  }

  showDialog(): void {
    if (
      this.companyDetailsSandbox.addressDetailsCurrentFormState.value.isMainAddress &&
      this.companyDetailsSandbox.existMainAddress &&
      !this.companyDetailsSandbox.isCurrentAddressMainAddress
    ) {
      const modalRef = this.modalService.open(SafetyQueryDialogComponent);
      modalRef.componentInstance.title = 'ConfirmDialog.Action.changeMainAddress';
      modalRef.componentInstance.body = 'ConfirmDialog.MainAddressModification';
      modalRef.result.then(
        () => {
          this.actionsSubject.pipe(ofType(companyActions.persistAddressDetailSuccess), take(1)).subscribe(() => {
            this.companyDetailsSandbox.persistAddress();
          });
          this.companyDetailsSandbox.removeCurrentMainAddress();
        },
        () => {}
      );
    } else {
      this.companyDetailsSandbox.persistAddress();
    }
  }
}
