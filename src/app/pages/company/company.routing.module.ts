/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Globals } from '@shared/constants/globals';
import { CompanyDetailsComponent } from '@pages/company/company-details/company-details.component';
import { CompanyDetailsResolver } from '@pages/company/company-details/company-details.resolver';
import { CompanyContactPersonDetailsComponent } from '@pages/company/company-details/contact-person-details/contact-person-details.component';
import { ContactPersonDetailsResolver } from '@pages/company/company-details/contact-person-details/contact-person-details.resolver';

const PATH = Globals.PATH;

const editCompanyRoutes: Routes = [
  {
    path: `:contactId`,
    component: CompanyDetailsComponent,
    resolve: {
      company: CompanyDetailsResolver,
    },
  },
];

const addNewCompanyRoutes: Routes = [
  {
    path: PATH.NEW,
    component: CompanyDetailsComponent,
    resolve: {
      externalPerson: CompanyDetailsResolver,
    },
  },
];

const contactPersonsRoutes: Routes = [
  {
    path: `:contactId/contact-person/:contactPersonId`,
    component: CompanyContactPersonDetailsComponent,
    resolve: {
      contactPersonsDetails: ContactPersonDetailsResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild([...addNewCompanyRoutes, ...editCompanyRoutes, ...contactPersonsRoutes])],
  exports: [RouterModule],
})
export class CompanyRoutingModule {}
