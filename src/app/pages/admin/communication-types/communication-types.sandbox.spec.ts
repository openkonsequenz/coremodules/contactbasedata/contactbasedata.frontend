import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommunicationTypesSandbox } from '@pages/admin/communication-types/communication-types.sandbox';
import { Store, ActionsSubject} from '@ngrx/store';
import { State } from '@shared/store';
import { UtilService } from '@shared/utility/utility.service';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import * as communicationTypesActions from '@shared/store/actions/admin/communication-types.action';
import { CommunicationType } from '@shared/models';

describe('CommunicationTypesSandbox', () => {
  let service: CommunicationTypesSandbox = {} as any;
  let appState: Store<State>;
  let actionSubject: ActionsSubject;
  let utilService: UtilService;
  let router: Router;
  let modalService: NgbModal;

  beforeEach(() => {
    appState = { dispatch:()=> {}, pipe: () => of(true), select:()=> of(true) } as any;
    actionSubject = {  pipe: () => of(true) } as any;
    utilService = { displayNotification() {} } as any;
    router = { navigateByUrl() {} } as any;
    modalService = { open() {} } as any;
    spyOn(appState, 'dispatch').and.callFake(() => {});

    service = new CommunicationTypesSandbox(appState, actionSubject, router, utilService, modalService);
  });

  it('should create CommunicationTypesSandbox service', () => {
    expect(service).toBeTruthy();
  });

  it('should set DisplayForm property', () => {
    service.setDisplayForm('x');
    expect(service.displayForm).toBeTruthy();
    expect(service.formMode).toBe('x');
  });

  it('should dispatch loadCommunicationTypes Action via loadCommunicationTypes()', () => {
    service.loadCommunicationTypes();
    expect(appState.dispatch).toHaveBeenCalledWith(communicationTypesActions.loadCommunicationTypes());
  });

  it('should dispatch loadCommunicationType Action via loadCommunicationType(id)', () => {
    service.loadCommunicationType('x');
    expect(appState.dispatch).toHaveBeenCalledWith(communicationTypesActions.loadCommunicationType({ payload: 'x' }));
  });

  it('should call dispatch for saving an communication type', () => {
    service.currentFormState = {isValid: true} as any;
    service.saveCommunicationType();
    expect(appState.dispatch).toHaveBeenCalledWith(communicationTypesActions.saveCommunicationType({
        payload: new CommunicationType()
      })
    );
  });

  it('should clear form via clear()', () => {
    service['clear']();
    expect(service.displayForm).toBe(false);
  });

  it('should open modal before deleting a salutation', () => {
    spyOn(service['modalService'], 'open')
      .and.returnValue({componentInstance: {title: ''}, result: {then: () => of(true)}} as any);
    service.deleteCommunicationType('x');
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should clear form state when current change is canceled and form state is pristine', () => {
    let spy = spyOn(service,'clear');
    service.currentFormState = {isPristine: true} as any;
    service.cancel();
    expect(spy).toHaveBeenCalled();
  });

  it('should open modal when current change is canceled and form state is not pristine', () => {
    spyOn(service['modalService'], 'open')
      .and.returnValue({componentInstance: {title: ''}, result: {then: () => of(true)}} as any);
    service.currentFormState = {isPristine: false} as any;
    service.cancel();
    expect(modalService.open).toHaveBeenCalled();
  });

});
