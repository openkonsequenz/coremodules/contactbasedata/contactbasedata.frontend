/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { UtilService } from '@shared/utility/utility.service';
import { Injectable } from '@angular/core';
import { Store, ActionsSubject } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import * as store from '@shared/store';
import * as communicationTypesActions from '@shared/store/actions/admin/communication-types.action';
import * as communicationTypesFormReducer from '@shared/store/reducers/admin/communication-types-details-form.reducer';
import { CommunicationType } from '@shared/models';
import { FormGroupState, SetValueAction, ResetAction, MarkAsTouchedAction } from 'ngrx-forms';
import { ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { takeUntil, take } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { ILoadCommunicationTypesSuccess } from '@shared/store/actions/admin/communication-types.action';

@Injectable()
export class CommunicationTypesSandbox extends BaseSandbox {
  public communicationTypes$ = this.appState$.select(store.getCommunicationTypesData);
  public communicationTypesLoading$ = this.appState$.select(store.getCommunicationTypesLoading);
  public formState$ = this.appState$.select(store.getCommunicationTypesDetails);

  public currentFormState: FormGroupState<CommunicationType>;
  public displayForm: boolean = false;
  public formMode: string;

  constructor(
    protected appState$: Store<store.State>,
    protected actionsSubject: ActionsSubject,
    protected router: Router,
    protected utilService: UtilService,
    protected modalService: NgbModal
  ) {
    super(appState$);
  }

  public setDisplayForm(mode?: string): void {
    this.clear();
    this.appState$.dispatch(new MarkAsTouchedAction(communicationTypesFormReducer.FORM_ID));
    this.displayForm = true;
    this.formMode = mode;
  }

  public loadCommunicationTypes(): Observable<ILoadCommunicationTypesSuccess> {
    this.appState$.dispatch(communicationTypesActions.loadCommunicationTypes());
    return this.actionsSubject.pipe(ofType(communicationTypesActions.loadCommunicationTypesSuccess), take(1));
  }

  public loadCommunicationType(id: string): void {
    this.appState$.dispatch(communicationTypesActions.loadCommunicationType({ payload: id }));
  }

  public deleteCommunicationType(id: string): void {
    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.appState$.dispatch(communicationTypesActions.deleteCommunicationType({ payload: id }));
        this.clear();
      },
      () => {}
    );
  }

  public cancel(): void {
    if (!this.currentFormState.isPristine) {
      const modalRef = this.modalService.open(SafetyQueryDialogComponent);
      modalRef.componentInstance.title = 'ConfirmDialog.Action.edit';
      modalRef.componentInstance.body = 'ConfirmDialog.Content';
      modalRef.result.then(
        () => {
          this.clear();
        },
        () => {}
      );
    } else {
      this.clear();
    }
  }

  clear(): void {
    this.appState$.dispatch(new SetValueAction(communicationTypesFormReducer.FORM_ID, communicationTypesFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(communicationTypesFormReducer.FORM_ID));
    this.displayForm = false;
  }

  public saveCommunicationType(): void {
    if (this.currentFormState.isValid) {
      this.appState$.dispatch(
        communicationTypesActions.saveCommunicationType({
          payload: new CommunicationType(this.currentFormState.value),
        })
      );
      this.actionsSubject.pipe(ofType(communicationTypesActions.saveCommunicationTypeSuccess), takeUntil(this._endSubscriptions$)).subscribe(() => {
        this.clear();
      });
    } else {
      this.utilService.displayNotification('MandatoryFieldError', 'alert');
    }
  }

  public registerEvents(): void {
    this.formState$.pipe(takeUntil(this._endSubscriptions$)).subscribe((formState: FormGroupState<CommunicationType>) => (this.currentFormState = formState));
  }
}
