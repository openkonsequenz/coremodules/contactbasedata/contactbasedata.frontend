 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Salutation } from '@shared/models';
import { SalutationsService } from '@pages/admin/salutations/salutations.service';

describe('SalutationsService', () => {
  let service: SalutationsService;

  beforeEach(() => {
  });

  it('should transform salutations list', () => {
    const response = [new Salutation()];
    response[0].type = 'Herr';

    expect(SalutationsService.gridAdapter(response)[0].type).toBe('Herr');
  });


  it('should transform salutations details', () => {
    const response: any = { type: 'Herr'};
    const salutation = SalutationsService.salutationAdapter(response);

    expect(salutation.type).toBe(response.type);
  });
});
