/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AddressTypesDetailsComponent } from '@pages/admin/address-types/address-types-details/address-types-details.component';
import { AddressTypesSandbox } from '@pages/admin/address-types//address-types.sandbox';

describe('AddressTypesDetailsComponent', () => {
  let component: AddressTypesDetailsComponent;
  let sandbox: AddressTypesSandbox;

  beforeEach(() => {
    sandbox = {
      registerEvents() {},
      endSubscriptions() {},
      clear() {},
    } as any;
    component = new AddressTypesDetailsComponent(sandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should registerEvents OnInit', () => {
    let spy = spyOn(sandbox,'registerEvents');
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should endSubscriptions OnDestroy', () => {
    let spy = spyOn(sandbox,'endSubscriptions');
    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });
});
