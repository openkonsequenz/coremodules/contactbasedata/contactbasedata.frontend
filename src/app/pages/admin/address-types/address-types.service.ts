/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { AddressType } from '@shared/models';

@Injectable()
export class AddressTypesService {

  static gridAdapter(response: any): Array<AddressType> {
    return response.map(addressType => new AddressType(addressType));
  }

  static itemAdapter(response: any): AddressType {
    return new AddressType(response);
  }
}
