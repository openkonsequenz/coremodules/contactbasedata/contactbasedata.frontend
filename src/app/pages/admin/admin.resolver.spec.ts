import { async } from '@angular/core/testing';
import { AdminResolver } from '@pages/admin/admin.resolver';

describe('AdminResolver', () => {
  let component: AdminResolver;
  let communicationTypesSandbox: any = {
    setDisplayForm: () => {},
    loadCommunicationTypes: () => {},
    deleteCommunicationType: () => {},
  };
  let personTypesSandbox: any = {
    setDisplayForm: () => {},
    loadPersonTypes: () => {},
    deletePersonType: () => {},
  };
  let addressTypesSandbox: any = {
    setDisplayForm: () => {},
    loadAddressTypes: () => {},
    deleteAddressType: () => {},
  };
  const salutationsSandbox: any = {
    setDisplayForm: () => {},
    loadSalutations: () => {},
    deleteSalutation: () => {},
  };

  beforeEach(() => {
    component = new AdminResolver(salutationsSandbox, communicationTypesSandbox, personTypesSandbox, addressTypesSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call loadSalutations', () => {
    const spy = spyOn(salutationsSandbox, 'loadSalutations');
    component.resolve();
    expect(spy).toHaveBeenCalled();
  });
});
