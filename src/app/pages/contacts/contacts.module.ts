/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ContactsSandbox } from '@pages/contacts/contacts.sandbox';
import { ContactsRoutingModule } from '@pages/contacts/contacts-routing.module';
import { ContactsListComponent } from '@pages/contacts/contacts-list/contacts-list.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ComponentsModule } from '@shared/components';
import { TranslateModule } from '@ngx-translate/core';
import { NgrxFormsModule } from 'ngrx-forms';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { DirectivesModule } from '@shared/directives';
import { ContainersModule } from '@shared/containers';
import { ContactsService } from '@pages/contacts/contacts.service';
import { ContactsApiClient } from '@pages/contacts/contacts-api-client';
import { EffectsModule } from '@ngrx/effects';
import { ContactsEffects } from '@shared/store/effects/contacts.effect';
import { SalutationsEffects } from '@app/shared/store/effects/admin/salutations.effect';
import { CommunicationTypesEffects } from '@app/shared/store/effects/admin/communication-types.effect';
import { PersonTypesEffects } from '@app/shared/store/effects/admin/person-types.effect';
import { AddressTypesEffects } from '@app/shared/store/effects/admin/address-types.effect';
import { CommunicationTypesApiClient } from '../admin/communication-types/communication-types-api-client';
import { PersonTypesApiClient } from '../admin/person-types/person-types-api-client';
import { AddressTypesApiClient } from '../admin/address-types/address-types-api-client';
import { SalutationsApiClient } from '../admin/salutations/salutations-api-client';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    AgGridModule,
    TranslateModule,
    DirectivesModule,
    ReactiveFormsModule,
    RouterModule,
    NgrxFormsModule,
    FormsModule,
    EffectsModule.forFeature([SalutationsEffects, CommunicationTypesEffects, PersonTypesEffects, AddressTypesEffects, ContactsEffects]),
    ContainersModule,
    ContactsRoutingModule,
  ],
  declarations: [ContactsListComponent],
  providers: [
    ContactsSandbox,
    ContactsService,
    ContactsApiClient,
    CommunicationTypesApiClient,
    PersonTypesApiClient,
    AddressTypesApiClient,
    SalutationsApiClient,
  ],
})
export class ContactsModule {}
