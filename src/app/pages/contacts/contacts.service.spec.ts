 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ContactsService } from '@pages/contacts/contacts.service';
import { PageModel } from './../../shared/models/page/page.model';
import { Contact } from '@shared/models';



describe('ContactsService ', () => {
  let service: ContactsService;


  beforeEach(() => {
  });

  it('should provide gridPageAdapter', () => {
    const pagemodel = new PageModel<Contact>();
    pagemodel.pageSize = 667;

    expect(ContactsService.gridPageAdapter( pagemodel ).pageSize).toBe(667);
  });


  it('should transform contact details', () => {
    const abstractContact: any = { lastName: 'Heysterkamp'};
    const transContact = ContactsService.contactDetailsAdapter(abstractContact);

    expect(transContact.lastName).toBe(abstractContact.lastName);
  });
});
