/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CONTACTS_COLDEF } from '@pages/contacts/contacts-list/contacts-list-column-definition';
import { ContactsSandbox } from '@pages/contacts/contacts.sandbox';
import { BaseList } from '@shared/components/base-components/base.list';
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
import { Globals } from '@shared/constants/globals';
import { ModifiedContacts } from '@shared/models/modifiedContacts.model';
import { ColumnMovedEvent, GridReadyEvent } from 'ag-grid-community';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss'],
})
export class ContactsListComponent extends BaseList implements OnInit, OnDestroy {
  public readonly NEW_EXTERNAL_PERSON_PATH = `/${Globals.PATH.PERSONS}${Globals.PATH.EXTERNAL}/${Globals.PATH.NEW}`;
  public readonly NEW_INTERNAL_PERSON_PATH = `/${Globals.PATH.PERSONS}${Globals.PATH.INTERNAL}/${Globals.PATH.NEW}`;
  public readonly NEW_COMPANY_PATH = `/${Globals.PATH.COMPANY}/${Globals.PATH.NEW}`;

  public readonly INTERNAL_PERSON = Globals.CONTACT_TYPE_ID.INTERNAL_PERSON;
  public readonly EXTERNAL_PERSON = Globals.CONTACT_TYPE_ID.EXTERNAL_PERSON;
  public readonly COMPANY = Globals.CONTACT_TYPE_ID.COMPANY;
  public readonly CONTACT_PERSON = Globals.CONTACT_TYPE_ID.CONTACT_PERSON;

  public columnDefinition: any = CONTACTS_COLDEF;
  public modifiedContacts: ModifiedContacts = new ModifiedContacts();
  public modifiedContactsTrigger$: Subject<ModifiedContacts> = new Subject<ModifiedContacts>();
  public isDSGVOFilterAdvancedVisible = false;
  public moduleDisplayName: string = '';
  public numberOfSubstrings: number = 0;

  private _sortingOrder: string = 'asc';
  private _standardSorting = 'name,id';
  private _sortingContactType: string = '';
  private _expiringDataInPast = false;
  private _withSyncError = false;
  private _deletionLockExceeded = false;
  private _subscription: Subscription;

  constructor(
    public contactsSandbox: ContactsSandbox,
    public userModuleAssignmentSandbox: UserModuleAssignmentSandBox,
    private router: Router,
    private ngZone: NgZone
  ) {
    super();
  }
  ngOnInit() {
    this.gridOptions = {
      ...this.gridOptions,
      localeText: Globals.LOCALE_TEXT,
    };
    // save / retrieve column positions
    this.gridOptions.onColumnMoved = this._saveColumnPositions.bind(this);
    this.gridOptions.onGridReady = this._restoreColumnPositions.bind(this);
    this.gridOptions.onGridReady = this.initGridData.bind(this);
    // context
    this.gridOptions.context = {
      ...this.gridOptions.context,
      icons: { edit: true, delete: false },
    };
    // handling icon events
    this._subscription = this.gridOptions.context.eventSubject.subscribe(this._handleBusEvents.bind(this));
    // save filter / search config
  }

  private initGridData() {
    const data = JSON.parse(sessionStorage.getItem(Globals.SESSSION_STORAGE_KEYS.contactListSearchFilterKey));
    if (data) {
      this._updateModifiedContacts(data);
      if (this.modifiedContacts.sort) {
        //sonst wird bei rückkehr aus navigation die sort property null gesetzt (via _setModifiedContactsSort)
        this.numberOfSubstrings = (this.modifiedContacts.sort.match(/,/g) || []).length;
        this._sortingContactType = this.modifiedContacts.sort.split(',')[0];
        this._sortingOrder = this.modifiedContacts.sort.split(',')[this.numberOfSubstrings];
      }
      this.isDSGVOFilterAdvancedVisible = this.modifiedContacts.isDSGVOFilterAdvancedVisible;
      this._expiringDataInPast = this.modifiedContacts.expiringDataInPast;
      this._withSyncError = this.modifiedContacts.withSyncError;
      this._deletionLockExceeded = this.modifiedContacts.deletionLockExceeded;
      // modifiedContacts modulWerte zurueckuebersetzen in Display modulWerte
      this.setModifiedContactsModuleAssignmentFilterDisplayValues(this.modifiedContacts.moduleName, this.modifiedContacts.withoutAssignedModule);
    }
  }

  private _saveColumnPositions(event: ColumnMovedEvent) {
    let columnState = JSON.stringify(event.api.getColumnState());
    sessionStorage.setItem(Globals.SESSSION_STORAGE_KEYS.contactListColumnStateKey, columnState);
  }

  private _restoreColumnPositions(event: GridReadyEvent) {
    let columnState = JSON.parse(sessionStorage.getItem(Globals.SESSSION_STORAGE_KEYS.contactListColumnStateKey));
    if (columnState) {
      event.api.applyColumnState({ state: columnState, applyOrder: true });
    }
  }

  private _handleBusEvents(event: any): any {
    switch (event.type) {
      case 'readonly':
        this.navigateToDetails(event);
      case 'edit':
        this.navigateToDetails(event);
        break;
      default:
        break;
    }
  }

  public navigateTo(url: any) {
    this.ngZone.run(() => this.router.navigate([url]));
  }

  public navigateToDetails(event: any) {
    let route;
    switch (event.data.contactType) {
      case Globals.CONTACT_TYPE_ID.EXTERNAL_PERSON:
        route = `/${Globals.PATH.PERSONS}${Globals.PATH.EXTERNAL}/`;
        break;
      case Globals.CONTACT_TYPE_ID.INTERNAL_PERSON:
        route = `/${Globals.PATH.PERSONS}${Globals.PATH.INTERNAL}/`;
        break;
      case Globals.CONTACT_TYPE_ID.CONTACT_PERSON:
        route = `/${Globals.PATH.COMPANY}/${event.data.companyId}/${Globals.PATH.CONTACT_PERSON}/`;
        break;
      case Globals.CONTACT_TYPE_ID.COMPANY:
        route = `/${Globals.PATH.COMPANY}/`;
        break;
      default:
        route = '/overview';
        break;
    }

    this.ngZone.run(() => this.router.navigate(route !== '/overview' ? [route, event.data.uuid] : [route]));
  }

  public setModifiedContactsSearchText(searchText: string) {
    const updatedModifiedContacts = { ...this.modifiedContacts, searchText };
    this._updateModifiedContacts(updatedModifiedContacts);
  }

  public setModifiedContactsContactTypeId(contactTypeId: string) {
    const updatedModifiedContacts = { ...this.modifiedContacts, contactTypeId };
    this._updateModifiedContacts(updatedModifiedContacts);
  }

  public setModifiedContactsModuleAssignmentFilter(moduleName: string) {
    let updatedModifiedContacts;
    if (moduleName === '-1') {
      //show contacts without module assignments
      updatedModifiedContacts = { ...this.modifiedContacts, moduleName: null, withoutAssignedModule: true };
    } else if (moduleName == '') {
      //show contacts with all module assignments
      updatedModifiedContacts = { ...this.modifiedContacts, moduleName: null, withoutAssignedModule: false };
    } else {
      //show contacts with specific module assignments
      updatedModifiedContacts = { ...this.modifiedContacts, moduleName: moduleName, withoutAssignedModule: false };
    }
    this._updateModifiedContacts(updatedModifiedContacts);
  }

  public setModifiedContactsModuleAssignmentFilterDisplayValues(moduleName: string, withoutAssignedModule: boolean) {
    if (!moduleName && withoutAssignedModule) {
      //show contacts without module assignments
      this.moduleDisplayName = '-1';
    } else if (!moduleName && !withoutAssignedModule) {
      //show contacts with all module assignments
      this.moduleDisplayName = '';
    } else {
      //show contacts with specific module assignments
      this.moduleDisplayName = moduleName;
    }
  }

  public setModifiedContactsExpiryDateFilter() {
    this._expiringDataInPast = !this._expiringDataInPast;
    const updatedModifiedContacts = { ...this.modifiedContacts, expiringDataInPast: this._expiringDataInPast };
    this._updateModifiedContacts(updatedModifiedContacts);
  }

  public setModifiedContactsWithSyncErrorFilter() {
    this._withSyncError = !this._withSyncError;
    const updatedModifiedContacts = { ...this.modifiedContacts, withSyncError: this._withSyncError };
    this._updateModifiedContacts(updatedModifiedContacts);
  }

  public setModifiedContactsDeletionLockExceedFilter() {
    this._deletionLockExceeded = !this._deletionLockExceeded;
    const updatedModifiedContacts = { ...this.modifiedContacts, deletionLockExceeded: this._deletionLockExceeded };
    this._updateModifiedContacts(updatedModifiedContacts);
  }

  public setSortingContactType(sortingContactType: string) {
    this._sortingContactType = sortingContactType;
  }

  public setSortingOrder(sortingOrder: string) {
    this._sortingOrder = sortingOrder;
  }

  public searchContacts(event: KeyboardEvent) {
    if (event.type === 'change' || event.key === 'Enter') {
      this._updateModifiedContacts(this.modifiedContacts);
    } else if (event.type == 'keyup') {
      setTimeout(() => {
        this._updateModifiedContacts(this.modifiedContacts);
      }, 1000);
    }
  }

  public setDSGVOFilterAdvancedVisible() {
    this.isDSGVOFilterAdvancedVisible = !this.isDSGVOFilterAdvancedVisible;
    if (
      !this.isDSGVOFilterAdvancedVisible &&
      (this.modifiedContacts.moduleName != null ||
        this.modifiedContacts.withoutAssignedModule ||
        this.modifiedContacts.deletionLockExceeded ||
        this.modifiedContacts.expiringDataInPast)
    ) {
      this._resetDSGVOFilter();
    }
    const updatedModifiedContacts = { ...this.modifiedContacts, isDSGVOFilterAdvancedVisible: this.isDSGVOFilterAdvancedVisible };
    this._updateModifiedContacts(updatedModifiedContacts, false);
  }

  private _resetDSGVOFilter() {
    this._deletionLockExceeded = false;
    this._expiringDataInPast = false;
    const updatedModifiedContacts = {
      ...this.modifiedContacts,
      moduleName: null,
      withoutAssignedModule: false,
      deletionLockExceeded: false,
      expiringDataInPast: false,
    };
    this._updateModifiedContacts(updatedModifiedContacts, false);
  }

  public sortContacts() {
    if (this._sortingOrder && this._sortingContactType) {
      const sort = this._sortingContactType + ',' + this._standardSorting + ',' + this._sortingOrder;
      const updatedModifiedContacts = { ...this.modifiedContacts, sort };
      this.numberOfSubstrings = (updatedModifiedContacts.sort.match(/,/g) || []).length;
      this._updateModifiedContacts(updatedModifiedContacts);
    } else {
      const updatedModifiedContacts = { ...this.modifiedContacts, sort: null };
      this._updateModifiedContacts(updatedModifiedContacts);
    }
  }

  private _updateModifiedContacts(updatedModifiedContacts: ModifiedContacts, loadContacts: boolean = true) {
    this.modifiedContacts = updatedModifiedContacts;
    sessionStorage.setItem(Globals.SESSSION_STORAGE_KEYS.contactListSearchFilterKey, JSON.stringify(this.modifiedContacts));
    if (!loadContacts) {
      return;
    }
    this.modifiedContactsTrigger$.next(updatedModifiedContacts);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
