/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ExternalPersonDetailsSandBox } from '@pages/persons/external-person/external-person-details/external-person-details.sandbox';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { SalutationsSandbox } from '@pages/admin/salutations/salutations.sandbox';
import { CommunicationTypesSandbox } from '@pages/admin/communication-types/communication-types.sandbox';
import { AddressTypesSandbox } from '@pages/admin/address-types/address-types.sandbox';
import { PersonTypesSandbox } from '@pages/admin/person-types/person-types.sandbox';
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
@Injectable()
export class ExternalPersonResolver  {
  constructor(
    private _externalPersonSandbox: ExternalPersonDetailsSandBox,
    private _salutationsSandbox: SalutationsSandbox,
    private _userModuleAssignmentSandbox: UserModuleAssignmentSandBox,
    private _communicationTypesSandbox: CommunicationTypesSandbox,
    private _personTypesSandbox: PersonTypesSandbox,
    private _addressTypesSandbox: AddressTypesSandbox
  ) {}

  /**
   * @param route
   */
  public resolve(route: ActivatedRouteSnapshot): void {
    const contactId: string = route.params['contactId'];
    if (contactId && contactId !== 'new') {
      this._externalPersonSandbox.loadExternalPerson(contactId);
      this._externalPersonSandbox.loadExternalPersonAddresses(contactId);
      this._externalPersonSandbox.loadCommunicationsData(contactId);
      this._userModuleAssignmentSandbox.loadUserModuleAssignments(contactId);
    } else {
      this._externalPersonSandbox.newExternalPerson();
    }

    this._salutationsSandbox.loadSalutations();
    this._personTypesSandbox.loadPersonTypes();
    this._communicationTypesSandbox.loadCommunicationTypes();
    this._addressTypesSandbox.loadAddressTypes();
    this._userModuleAssignmentSandbox.loadFilteredUserModuleTypes();
    this._externalPersonSandbox.isAddressDataDetailViewVisible = false;
    this._externalPersonSandbox.isCommunicationsDataDetailViewVisible = false;
  }
}
