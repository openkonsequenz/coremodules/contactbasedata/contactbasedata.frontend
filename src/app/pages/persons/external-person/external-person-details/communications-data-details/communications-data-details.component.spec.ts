/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { ExternalPersonCommunicationsDataDetailsComponent } from '@pages/persons/external-person/external-person-details/communications-data-details/communications-data-details.component';

describe('ExternalPersonCommunicationsDataDetailsComponent', () => {
  let component: ExternalPersonCommunicationsDataDetailsComponent;
  let externalPersonSandbox: any;
  let communicationTypesSandbox: any;

  beforeEach(async(() => {
    externalPersonSandbox = {
      registerCommunicationsDataEvents() {},
      registerExternalPersonEvents() {},
      endSubscriptions() {},
      newCommunicationsData() {},
    } as any;

    communicationTypesSandbox = {} as any;
  }));

  beforeEach(() => {
    component = new ExternalPersonCommunicationsDataDetailsComponent(externalPersonSandbox, communicationTypesSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call registerCommunicationsDataEvents and newCommunicationsData onInit', () => {
    const spy1 = spyOn(externalPersonSandbox, 'registerCommunicationsDataEvents');
    const spy2 = spyOn(externalPersonSandbox, 'newCommunicationsData');
    component.ngOnInit();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });
});
