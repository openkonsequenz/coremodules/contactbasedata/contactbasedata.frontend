/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ExternalPersonDetailsComponent } from '@pages/persons/external-person/external-person-details/external-person-details.component';

describe('ExternalPersonDetailsComponent', () => {
  let component: ExternalPersonDetailsComponent;
  let externalPersonSandbox: any;
  let userModuleAssignmentSandBox: any;
  let translate: any;
  let salutationsSandbox: any;
  let personTypesSandbox: any;

  beforeEach(() => {
    externalPersonSandbox = {
      registerExternalPersonEvents() {},
      endSubscriptions() {},
      loadCommunicationsDataDetails() {},
      loadExternalPersonDetailsAddressDetails() {},
    } as any;

    salutationsSandbox = {} as any;

    personTypesSandbox = {} as any;

    translate = {
      instant() {},
    } as any;

    component = new ExternalPersonDetailsComponent(externalPersonSandbox, userModuleAssignmentSandBox, salutationsSandbox, personTypesSandbox, translate);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should loadSalutations on init', () => {
    const spy = spyOn(externalPersonSandbox, 'registerExternalPersonEvents');
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should end all subscriptions for this sandbox onDestroy', () => {
    const spy = spyOn(externalPersonSandbox, 'endSubscriptions');
    component.ngOnDestroy();

    expect(spy).toHaveBeenCalled();
  });

  it('should call translate instant if no adress id', () => {
    const spy = spyOn(translate, 'instant');
    component.loadAddressDetail(null);

    expect(spy).toHaveBeenCalledWith('Address.NewAddress');
  });

  it('should call translate instant if there is an adress id', () => {
    const id = 'id';
    const spy = spyOn(translate, 'instant');
    const spy1 = spyOn(externalPersonSandbox, 'loadExternalPersonDetailsAddressDetails');
    component.loadAddressDetail(id);

    expect(spy).toHaveBeenCalledWith('Address.EditAddress');
    expect(spy1).toHaveBeenCalledWith(id);
  });

  it('should call translate instant if there is an adress id', () => {
    component.externalPersonSandBox.isCommunicationsDataDetailViewVisible = false;
    component.loadCommunicationsDataDetail(null);
    expect(component.externalPersonSandBox.isCommunicationsDataDetailViewVisible).toBeTruthy();
  });

  it('should call translate instant if there is id', () => {
    component.externalPersonSandBox.isAddressDataDetailViewVisible = false;
    component.loadAddressDetail(null);
    expect(component.externalPersonSandBox.isAddressDataDetailViewVisible).toBeTruthy();
  });

  it('should call translate instant if no communications data id', () => {
    const spy = spyOn(translate, 'instant');
    component.loadCommunicationsDataDetail(null);

    expect(spy).toHaveBeenCalledWith('CommunicationsData.NewCommunicationsData');
  });

  it('should call translate instant if there is an communications data id', () => {
    const id = 'id';
    const spy = spyOn(translate, 'instant');
    const spy1 = spyOn(externalPersonSandbox, 'loadCommunicationsDataDetails');
    component.loadCommunicationsDataDetail(id);

    expect(spy).toHaveBeenCalledWith('CommunicationsData.EditCommunicationsData');
    expect(spy1).toHaveBeenCalledWith(id);
  });

  it('should call translate instant if there is an communications data id', () => {
    component.externalPersonSandBox.isCommunicationsDataDetailViewVisible = false;
    component.loadCommunicationsDataDetail(null);
    expect(component.externalPersonSandBox.isCommunicationsDataDetailViewVisible).toBeTruthy();
  });

  it('should set expandableVisible to "true" if externalPersonContactId is defined', () => {
    component.externalPersonSandBox.externalPersonContactId = 'test';
    (component as any)._initExpandableState();
    expect(component.isExpandableVisible).toBeTruthy();
    expect(component.externalPersonSandBox.isAddressDataDetailViewVisible).toBeFalsy();
    expect(component.externalPersonSandBox.isCommunicationsDataDetailViewVisible).toBeFalsy();
  });
});
