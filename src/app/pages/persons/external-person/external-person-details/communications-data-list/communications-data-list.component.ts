/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ExternalPersonDetailsSandBox } from '@pages/persons/external-person/external-person-details/external-person-details.sandbox';
import { BaseList } from '@shared/components/base-components/base.list';
import { Component, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core';
import { COMMUNICATIONS_DATA_LIST_COLDEF } from '@shared/components/column-definitions/communications-data-list-column-definition';
import { Globals } from '@shared/constants/globals';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-external-person-communications-data-list',
  templateUrl: './communications-data-list.component.html',
  styleUrls: ['./communications-data-list.component.scss'],
})
export class ExternalPersonCommunicationsDataListComponent extends BaseList implements OnInit, OnDestroy {
  @Output() externalPersonDetailsIdLoaded: EventEmitter<string> = new EventEmitter();
  @Output() createNewExternalPerson: EventEmitter<string> = new EventEmitter();

  public columnDefinition = COMMUNICATIONS_DATA_LIST_COLDEF;
  private _subscription: Subscription;

  constructor(public externalPersonDetailsSandBox: ExternalPersonDetailsSandBox) {
    super();
  }

  ngOnInit() {
    this.gridOptions = {
      ...this.gridOptions,
      localeText: Globals.LOCALE_TEXT,
    };
    this.gridOptions.context = {
      ...this.gridOptions.context,
      icons: { edit: true, delete: true },
    };

    this._subscription = this.gridOptions.context.eventSubject.subscribe(event => {
      if (event.type === 'edit' || event.type === 'readonly') {
        this.externalPersonDetailsIdLoaded.emit(event.data.id);
      }
      if (event.type === 'delete') {
        this.externalPersonDetailsSandBox.deleteCommunicationsData(event.data);
      }
    });
  }

  public createNewCommunicationsDataForm() {
    this.externalPersonDetailsSandBox.clearCommunicationsData();
    this.createNewExternalPerson.emit(null);
    this.externalPersonDetailsSandBox.communicationsAgApi.setDomLayout('normal');
  }

  onGridReady(params) {
    this.externalPersonDetailsSandBox.communicationsAgApi = params.api;
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
