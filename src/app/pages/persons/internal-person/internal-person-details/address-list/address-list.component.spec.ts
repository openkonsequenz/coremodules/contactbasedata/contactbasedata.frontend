/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { InternalPersonAddressListComponent } from '@pages/persons/internal-person/internal-person-details/address-list/address-list.component';
import { of } from 'rxjs';

describe('AddressListComponent', () => {
  let component: InternalPersonAddressListComponent;
  let internalPersonSandbox: any;
  let agApi: any;

  beforeEach(async(() => {
    agApi = { setDomLayout() {} };
    internalPersonSandbox = {
      registerAddressEvents() {},
      registerExternalPersonEvents() {},
      endSubscriptions() {},
      deleteAddress() {},
      clearAddressData() {},
    } as any;
    internalPersonSandbox.addressAgApi = agApi;
  }));

  beforeEach(() => {
    component = new InternalPersonAddressListComponent(internalPersonSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define gridOptions onInit', () => {
    component.ngOnInit();

    expect(component.gridOptions).toBeDefined();
    expect(component.gridOptions.context).toBeDefined();
  });

  it('should emit if BusEvents is edit ', () => {
    const spy = spyOn(component.internalPersonDetailsId, 'emit');
    const event: any = { type: 'edit', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should deleteAddress if BusEvents is delete', () => {
    const spy = spyOn(internalPersonSandbox, 'deleteAddress');
    const event: any = { type: 'delete', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should clearAddressData and close edit area if callcreateNewAddressForm', () => {
    const spy = spyOn(internalPersonSandbox, 'clearAddressData');
    const spyEmit = spyOn(component.createNewInternalPerson, 'emit');
    component.createNewAddressForm();

    expect(spy).toHaveBeenCalled();
    expect(spyEmit).toHaveBeenCalled();
  });
});
