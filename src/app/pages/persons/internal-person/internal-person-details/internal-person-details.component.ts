/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FaIcons } from '@app/shared/constants/fa-icons';
import { TranslateService } from '@ngx-translate/core';
import { PersonTypesSandbox } from '@pages/admin/person-types/person-types.sandbox';
import { SalutationsSandbox } from '@pages/admin/salutations/salutations.sandbox';
import { InternalPersonDetailsSandBox } from '@pages/persons/internal-person/internal-person-details/internal-person-details.sandbox';
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
import { UserModuleAssignment } from '@shared/models';

@Component({
  selector: 'app-internal-person-details',
  templateUrl: './internal-person-details.component.html',
  styleUrls: ['./internal-person-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InternalPersonDetailsComponent implements OnInit, OnDestroy {
  public readonly FaIcons = FaIcons;

  public createOrEditCommunicationsData = '';
  public createOrEditAddressData = '';
  public createOrEditUserModuleAssignmentData = '';
  public isExpandableVisible = false;

  constructor(
    public internalPersonSandBox: InternalPersonDetailsSandBox,
    public userModuleAssignmentSandBox: UserModuleAssignmentSandBox,
    public salutationSandBox: SalutationsSandbox,
    public personTypesSandbox: PersonTypesSandbox,
    private _translate: TranslateService
  ) {}

  ngOnInit() {
    this.internalPersonSandBox.registerInternalPersonEvents();
    this._initExpandableState();
  }

  ngOnDestroy() {
    this.internalPersonSandBox.endSubscriptions();
    this.internalPersonSandBox.internalPersonContactId = null;
  }

  public loadCommunicationsDataDetail(detailId: string) {
    if (detailId == null) {
      this.createOrEditCommunicationsData = this._translate.instant('CommunicationsData.NewCommunicationsData');
    } else {
      this.createOrEditCommunicationsData = this._translate.instant('CommunicationsData.EditCommunicationsData');
      this.internalPersonSandBox.loadCommunicationsDataDetails(detailId);
    }

    this.internalPersonSandBox.isCommunicationsDataDetailViewVisible = true;
  }

  public loadAddressDetail(detailId: string) {
    if (detailId == null) {
      this.createOrEditAddressData = this._translate.instant('Address.NewAddress');
    } else {
      this.createOrEditAddressData = this._translate.instant('Address.EditAddress');
      this.internalPersonSandBox.loadInternalPersonDetailsAddressDetails(detailId);
    }

    this.internalPersonSandBox.isAddressDataDetailViewVisible = true;
  }

  public loadUserModuleAssignmentDataDetail(userModuleAssignment: UserModuleAssignment) {
    if (userModuleAssignment == null) {
      this.createOrEditUserModuleAssignmentData = this._translate.instant('UserModuleAssignment.NewUserModuleAssignment');
    } else {
      this.createOrEditUserModuleAssignmentData = this._translate.instant('UserModuleAssignment.EditUserModuleAssignment');
      this.userModuleAssignmentSandBox.loadUserModuleAssignmentDetails(userModuleAssignment);
    }
    this.userModuleAssignmentSandBox.isUserModuleAssignmentDataDetailViewVisible = true;
  }

  private _initExpandableState() {
    this.isExpandableVisible = !!this.internalPersonSandBox.internalPersonContactId;

    this.internalPersonSandBox.isAddressDataDetailViewVisible = false;
    this.internalPersonSandBox.isCommunicationsDataDetailViewVisible = false;
  }
}
