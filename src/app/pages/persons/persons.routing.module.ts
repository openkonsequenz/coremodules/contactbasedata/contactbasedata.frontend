/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ExternalPersonDetailsComponent } from '@pages/persons/external-person/external-person-details/external-person-details.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, UrlSegment, UrlSegmentGroup } from '@angular/router';
import { ExternalPersonResolver } from '@pages/persons/external-person/external-person.resolver';
import { Globals } from '@shared/constants/globals';
import { InternalPersonDetailsComponent } from '@pages/persons/internal-person/internal-person-details/internal-person-details.component';
import { InternalPersonResolver } from '@pages/persons/internal-person/internal-person.resolver';

const PATH = Globals.PATH;
export function isInternal(url: UrlSegment[], group: UrlSegmentGroup) {
  const sePath = group.segments[0].path;
  return sePath.endsWith(`${Globals.PATH.PERSONS}${Globals.PATH.INTERNAL}`) || sePath.endsWith(`${Globals.PATH.NEW}`)
    ? { consumed: url, posParams: { contactId: url[0] } }
    : null;
}
export function isExternal(url: UrlSegment[], group: UrlSegmentGroup) {
  const sePath = group.segments[0].path;
  return sePath.endsWith(`${Globals.PATH.PERSONS}${Globals.PATH.EXTERNAL}`) || sePath.endsWith(`${Globals.PATH.NEW}`)
    ? { consumed: url, posParams: { contactId: url[0] } }
    : null;
}
const editPersonRoutes: Routes = [
  {
    component: ExternalPersonDetailsComponent,
    resolve: {
      externalPerson: ExternalPersonResolver,
    },
    matcher: isExternal,
  },
  {
    component: InternalPersonDetailsComponent,
    resolve: {
      internalPerson: InternalPersonResolver,
    },
    matcher: isInternal,
  },
];

@NgModule({
  imports: [RouterModule.forChild(editPersonRoutes)],
  exports: [RouterModule],
})
export class PersonsRoutingModule {}
