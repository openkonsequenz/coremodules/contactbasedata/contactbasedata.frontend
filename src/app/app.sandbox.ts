/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { UserConstants } from '@shared/constants/user.constants';
import { User } from '@shared/models/user';
import { Injectable } from '@angular/core';
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import { Store } from '@ngrx/store';
import * as store from '@shared/store';
import * as settingsActions from '@shared/store/actions/settings.action';
import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from './app-config.service';

/**
 * App Snadbox
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @class AppSandbox
 * @extends {BaseSandbox}
 */
@Injectable()
export class AppSandbox extends BaseSandbox {
  constructor(protected appState$: Store<store.State>, private translate: TranslateService, private configService: ConfigService) {
    super(appState$);
  }

  /**
   * Sets up default language for the application. Uses browser default language.
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @memberof AppSandbox
   */
  public setupLanguage(): void {
    const localization: any = this.configService.get('localization');
    const languages: Array<string> = localization.languages.map(lang => lang.code);
    const selectedCulture = localization.languages.filter(lang => lang.code === localization.defaultLanguage)[0].culture;

    this.translate.addLangs(languages);

    this.translate.setDefaultLang(localization.defaultLanguage);

    this.translate.use(localization.defaultLanguage);

    this.appState$.dispatch(settingsActions.setLanguage({ payload: localization.defaultLanguage }));

    this.appState$.dispatch(settingsActions.setCulture({ payload: selectedCulture }));
  }

  /**
   * Returns global notification options
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @returns {*}
   * @memberof AppSandbox
   */
  public getNotificationOptions(): any {
    return this.configService.get('notifications').options;
  }

  /**
   * Sets current user to redux store and local storage to be available
   * also after page refresh
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @param {User} [user=null]
   * @memberof AppSandbox
   */
  public setUser(user: User = null): void {
    let storedUser: User;
    if (!!user) {
      localStorage.setItem(UserConstants.storageKey, JSON.stringify(user));
    } else {
      storedUser = JSON.parse(localStorage.getItem(UserConstants.storageKey));
    }
    this.appState$.dispatch(settingsActions.setUser({ payload: user || storedUser }));
  }
}
