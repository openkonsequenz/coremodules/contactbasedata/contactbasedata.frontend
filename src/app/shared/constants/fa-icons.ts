/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {
  faCalendar,
  faChevronDown,
  faChevronUp,
  faEdit,
  faExclamation,
  faEye,
  faHome,
  faInfoCircle,
  faQuestionCircle,
  faServer,
  faTimesCircle,
  faTrash,
  faUser,
} from '@fortawesome/free-solid-svg-icons';

export const FaIcons = {
  faEdit,
  faEye,
  faTrash,
  faHome,
  faServer,
  faQuestionCircle,
  faUser,
  faChevronUp,
  faChevronDown,
  faCalendar,
  faTimesCircle,
  faExclamation,
  faInfoCircle,
};
