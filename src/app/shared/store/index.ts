/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
import * as fromSettings from '@shared/store/reducers/settings.reducer';
import * as fromContactsPage from '@shared/store/reducers/contacts/contacts-page.reducer';
import * as fromExternalPersonForm from '@shared/store/reducers/persons/external-person/external-person-details-form.reducer';
import * as fromInternalPersonForm from '@shared/store/reducers/persons/internal-person/internal-person-details-form.reducer';
import {
  CommunicationType,
  UserModuleAssignment,
  ContactPerson,
  ExternalPerson,
  InternalPerson,
  Address,
  Company,
  Salutation,
  CommunicationsData,
  PersonType,
  AddressType,
  UserModuleType,
} from '@shared/models';
import { FormGroupState } from 'ngrx-forms';
import { createFeatureSelector } from '@ngrx/store';
import { createSelector } from 'reselect';
import * as fromSalutations from '@shared/store/reducers/admin/salutations.reducer';
import * as fromSalutationsDetailForm from '@shared/store/reducers/admin/salutations-details-form.reducer';
import * as fromCommunicationTypes from '@shared/store/reducers/admin/communication-types.reducer';
import * as fromCommunicationTypesDetailForm from '@shared/store/reducers/admin/communication-types-details-form.reducer';
import * as fromExternalPersonDetailsAddressesDetailsForm from '@shared/store/reducers/persons/external-person/addresses-details-form.reducer';
import * as fromInternalPersonDetailsAddressesDetailsForm from '@shared/store/reducers/persons/internal-person/addresses-details-form.reducer';
import * as fromCompanyDetailsAddressesDetailsForm from '@shared/store/reducers/company/addresses-details-form.reducer';
import * as fromCompanyDetailForm from '@shared/store/reducers/company/company-details-form.reducer';
import * as fromPersonTypes from '@shared/store/reducers/admin/person-types.reducer';
import * as fromPersonTypesDetailForm from '@shared/store/reducers/admin/person-types-details-form.reducer';
import * as fromAddressTypes from '@shared/store/reducers/admin/address-types.reducer';
import * as fromAddressTypesDetailForm from '@shared/store/reducers/admin/address-types-details-form.reducer';
import * as fromUserModuleTypes from '@shared/store/reducers/user-module-assignment/user-module-types.reducer';
import * as fromExternalPersonDetailsAddresses from '@shared/store/reducers/persons/external-person/addresses.reducer';
import * as fromExternalPersonDetailsCommunicationsData from '@shared/store/reducers/persons/external-person/communications-data.reducer';
import * as fromExternalPersonDetailsCommunicationsDataDetailsForm from '@shared/store/reducers/persons/external-person/communications-data-details-form.reducer';
import * as fromInternalPersonDetailsCommunicationsData from '@shared/store/reducers/persons/internal-person/communications-data.reducer';
import * as fromInternalPersonDetailsCommunicationsDataDetailsForm from '@shared/store/reducers/persons/internal-person/communications-data-details-form.reducer';
import * as fromCompanyDetailsCommunicationsData from '@shared/store/reducers/company/communications-data.reducer';
import * as fromCompanyDetailsCommunicationsDataDetailsForm from '@shared/store/reducers/company/communications-data-details-form.reducer';
import * as fromInternalPersonDetailsAddresses from '@shared/store/reducers/persons/internal-person/addresses.reducer';
import * as fromCompanyDetailsAddresses from '@shared/store/reducers/company/addresses.reducer';
import * as fromCompanyDetailsContactPersons from '@shared/store/reducers/company/contact-persons.reducer';
import * as fromCompanyDetailsContactPersonsDetailForm from '@shared/store/reducers/company/contact-person-details/contact-persons-details-form.reducer';
import * as fromContactPersonDetailsAddresses from '@shared/store/reducers/company/contact-person-details/addresses.reducer';
import * as fromContactPersonDetailsAddressesDetailsForm from '@shared/store/reducers/company/contact-person-details/addresses-details-form.reducer';
import * as fromContactPersonDetailsCommunicationsData from '@shared/store/reducers/company/contact-person-details/communications-data.reducer';
import * as fromContactPersonDetailsCommunicationsDataDetailsForm from '@shared/store/reducers/company/contact-person-details/communications-data-details-form.reducer';
import * as fromUserModuleAssignments from '@shared/store/reducers/user-module-assignment/user-module-assignment.reducer';
import * as fromUserModuleAssignmentDetailsForm from '@shared/store/reducers/user-module-assignment/user-module-assignment-details-form.reducer';
import * as fromKeycloakUser from '@shared/store/reducers/users/keycloak-user.reducer';
import * as fromLdapUser from '@shared/store/reducers/users/ldap-user.reducer';

/**
 * We treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface State {
  settings: fromSettings.State;
  contactsPage: fromContactsPage.State;
}

export interface PersonState {
  externalPersonDetailsForm: FormGroupState<ExternalPerson>;
  externalPersonDetailsAddresses: fromExternalPersonDetailsAddresses.State;
  internalPersonDetailsAddresses: fromInternalPersonDetailsAddresses.State;
  externalPersonDetailsAddressesDetailsForm: FormGroupState<Address>;
  internalPersonDetailsAddressesDetailsForm: FormGroupState<Address>;
  internalPersonDetailsForm: FormGroupState<InternalPerson>;
  externalPersonDetailsCommunicationsData: fromExternalPersonDetailsCommunicationsData.State;
  externalPersonDetailsCommunicationsDataDetailsForm: FormGroupState<CommunicationsData>;
  internalPersonDetailsCommunicationsData: fromInternalPersonDetailsCommunicationsData.State;
  internalPersonDetailsCommunicationsDataDetailsForm: FormGroupState<CommunicationsData>;
  keycloakUsers: fromKeycloakUser.State;
  ldapUsers: fromLdapUser.State;
}

export interface AdminState {
  salutations: fromSalutations.State;
  salutationsDetailForm: FormGroupState<Salutation>;
  communicationTypes: fromCommunicationTypes.State;
  communicationTypesDetailForm: FormGroupState<CommunicationType>;
  personTypes: fromPersonTypes.State;
  personTypesDetailForm: FormGroupState<PersonType>;
  addressTypes: fromAddressTypes.State;
  addressTypesDetailForm: FormGroupState<AddressType>;
}

export interface CompanyState {
  companyDetailsForm: FormGroupState<Company>;
  companyDetailsCommunicationsData: fromCompanyDetailsCommunicationsData.State;
  companyDetailsCommunicationsDataDetailsForm: FormGroupState<CommunicationsData>;
  companyDetailsAddresses: fromCompanyDetailsAddresses.State;
  companyDetailsAddressesDetailsForm: FormGroupState<Address>;
  companyDetailsContactPersons: fromCompanyDetailsContactPersons.State;
  companyDetailsContactPersonDetailsForm: FormGroupState<ContactPerson>;
  contactPersonDetailsAddresses: fromContactPersonDetailsAddresses.State;
  contactPersonDetailsAddressesDetailsForm: FormGroupState<Address>;
  contactPersonDetailsCommunicationsData: fromContactPersonDetailsCommunicationsData.State;
  contactPersonDetailsCommunicationsDataDetailsForm: FormGroupState<CommunicationsData>;
}

export interface UserModuleAssignmentState {
  userModuleAssignments: fromUserModuleAssignments.State;
  userModuleAssignmentDetailsForm: FormGroupState<UserModuleAssignment>;
  userModuleTypes: fromUserModuleTypes.State;
}

/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */
export const reducers = {
  settings: fromSettings.reducer,
  contactsPage: fromContactsPage.reducer,
};

export const adminReducers = {
  salutations: fromSalutations.reducer,
  salutationsDetailForm: fromSalutationsDetailForm.reducer,
  communicationTypes: fromCommunicationTypes.reducer,
  communicationTypesDetailForm: fromCommunicationTypesDetailForm.reducer,
  personTypes: fromPersonTypes.reducer,
  personTypesDetailForm: fromPersonTypesDetailForm.reducer,
  addressTypes: fromAddressTypes.reducer,
  addressTypesDetailForm: fromAddressTypesDetailForm.reducer,
};

export const personsReducers = {
  externalPersonDetailsForm: fromExternalPersonForm.reducer,
  externalPersonDetailsAddresses: fromExternalPersonDetailsAddresses.reducer,
  internalPersonDetailsAddresses: fromInternalPersonDetailsAddresses.reducer,
  externalPersonDetailsAddressesDetailsForm: fromExternalPersonDetailsAddressesDetailsForm.reducer,
  internalPersonDetailsAddressesDetailsForm: fromInternalPersonDetailsAddressesDetailsForm.reducer,
  internalPersonDetailsForm: fromInternalPersonForm.reducer,
  externalPersonDetailsCommunicationsData: fromExternalPersonDetailsCommunicationsData.reducer,
  externalPersonDetailsCommunicationsDataDetailsForm: fromExternalPersonDetailsCommunicationsDataDetailsForm.reducer,
  internalPersonDetailsCommunicationsData: fromInternalPersonDetailsCommunicationsData.reducer,
  internalPersonDetailsCommunicationsDataDetailsForm: fromInternalPersonDetailsCommunicationsDataDetailsForm.reducer,
  keycloakUsers: fromKeycloakUser.reducer,
  ldapUsers: fromLdapUser.reducer,
};

export const companyReducers = {
  companyDetailsForm: fromCompanyDetailForm.reducer,
  companyDetailsCommunicationsData: fromCompanyDetailsCommunicationsData.reducer,
  companyDetailsCommunicationsDataDetailsForm: fromCompanyDetailsCommunicationsDataDetailsForm.reducer,
  companyDetailsAddresses: fromCompanyDetailsAddresses.reducer,
  companyDetailsAddressesDetailsForm: fromCompanyDetailsAddressesDetailsForm.reducer,
  companyDetailsContactPersons: fromCompanyDetailsContactPersons.reducer,
  companyDetailsContactPersonDetailsForm: fromCompanyDetailsContactPersonsDetailForm.reducer,
  contactPersonDetailsAddresses: fromContactPersonDetailsAddresses.reducer,
  contactPersonDetailsAddressesDetailsForm: fromContactPersonDetailsAddressesDetailsForm.reducer,
  contactPersonDetailsCommunicationsData: fromContactPersonDetailsCommunicationsData.reducer,
  contactPersonDetailsCommunicationsDataDetailsForm: fromContactPersonDetailsCommunicationsDataDetailsForm.reducer,
};

export const userModuleAssignmentReducers = {
  userModuleAssignments: fromUserModuleAssignments.reducer,
  userModuleAssignmentDetailsForm: fromUserModuleAssignmentDetailsForm.reducer,
  userModuleTypes: fromUserModuleTypes.reducer,
};

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them useable, we
 * need to make new selectors that wrap them.
 */

/**
 * Settings store functions
 */
export const getSettingsState = (state: State) => state.settings;

export const getSelectedLanguage = createSelector(getSettingsState, fromSettings.getSelectedLanguage);
export const getSelectedCulture = createSelector(getSettingsState, fromSettings.getSelectedCulture);
export const getAvailableLanguages = createSelector(getSettingsState, fromSettings.getAvailableLanguages);

/**
 * Contacts Page store functions
 */
export const getContactsPageState = (state: State) => state.contactsPage;
export const getContactsPageLoaded = createSelector(getContactsPageState, fromContactsPage.getLoaded);
export const getContactsPageLoading = createSelector(getContactsPageState, fromContactsPage.getLoading);
export const getContactsPageFailed = createSelector(getContactsPageState, fromContactsPage.getFailed);
export const getContactsPageData = createSelector(getContactsPageState, fromContactsPage.getData);
/**
 * Settings store functions
 */
export const getUser = createSelector(getSettingsState, fromSettings.getUser);
/**
 * Persons store functions
 */
export const selectPersonState = createFeatureSelector<PersonState>('personsData');

export const selectExternalPersonDetails = createSelector(selectPersonState, (state: PersonState) => state.externalPersonDetailsForm);
export const getExternalPersonDetails = createSelector(selectExternalPersonDetails, fromExternalPersonForm.getFormState);

export const selectInternalPersonDetails = createSelector(selectPersonState, (state: PersonState) => state.internalPersonDetailsForm);
export const getInternalPersonDetails = createSelector(selectInternalPersonDetails, fromInternalPersonForm.getFormState);

/**
 * External Persons Addresses store functions
 */
export const selectExternalPersonAddresses = createSelector(selectPersonState, (state: PersonState) => state.externalPersonDetailsAddresses);
export const getExternalPersonAddressesLoaded = createSelector(selectExternalPersonAddresses, fromExternalPersonDetailsAddresses.getLoaded);
export const getExternalPersonAddressesLoading = createSelector(selectExternalPersonAddresses, fromExternalPersonDetailsAddresses.getLoading);
export const getExternalPersonAddressesFailed = createSelector(selectExternalPersonAddresses, fromExternalPersonDetailsAddresses.getFailed);
export const getExternalPersonAddressesData = createSelector(selectExternalPersonAddresses, fromExternalPersonDetailsAddresses.getData);

/**
 * External Person Addresses Details store functions
 */
export const selectExternalPersonAddressesDetails = createSelector(selectPersonState, (state: PersonState) => state.externalPersonDetailsAddressesDetailsForm);
export const getExternalPersonAddressesDetails = createSelector(
  selectExternalPersonAddressesDetails,
  fromExternalPersonDetailsAddressesDetailsForm.getFormState
);

/**
 * Admin store functions
 */
export const selectAdminState = createFeatureSelector<AdminState>('admin');
// salutations list
export const selectSalutations = createSelector(selectAdminState, (state: AdminState) => state.salutations);
export const getSalutationsLoaded = createSelector(selectSalutations, fromSalutations.getLoaded);
export const getSalutationsLoading = createSelector(selectSalutations, fromSalutations.getLoading);
export const getSalutationsFailed = createSelector(selectSalutations, fromSalutations.getFailed);
export const getSalutationsData = createSelector(selectSalutations, fromSalutations.getData);
// salutations details
export const selectSalutationDetails = createSelector(selectAdminState, (state: AdminState) => state.salutationsDetailForm);
export const getSalutationDetails = createSelector(selectSalutationDetails, fromSalutationsDetailForm.getFormState);
// communication types list
export const selectCommunicationTypes = createSelector(selectAdminState, (state: AdminState) => state.communicationTypes);

export const getCommunicationTypesLoaded = createSelector(selectCommunicationTypes, fromCommunicationTypes.getLoaded);
export const getCommunicationTypesLoading = createSelector(selectCommunicationTypes, fromCommunicationTypes.getLoading);
export const getCommunicationTypesFailed = createSelector(selectCommunicationTypes, fromCommunicationTypes.getFailed);
export const getCommunicationTypesData = createSelector(selectCommunicationTypes, fromCommunicationTypes.getData);
// communication types details
export const selectCommunicationTypesDetails = createSelector(selectAdminState, (state: AdminState) => state.communicationTypesDetailForm);
export const getCommunicationTypesDetails = createSelector(selectCommunicationTypesDetails, fromCommunicationTypesDetailForm.getFormState);
// person types list
export const selectPersonTypes = createSelector(selectAdminState, (state: AdminState) => state.personTypes);
export const getPersonTypesLoaded = createSelector(selectPersonTypes, fromPersonTypes.getLoaded);
export const getPersonTypesLoading = createSelector(selectPersonTypes, fromPersonTypes.getLoading);
export const getPersonTypesFailed = createSelector(selectPersonTypes, fromPersonTypes.getFailed);
export const getPersonTypesData = createSelector(selectPersonTypes, fromPersonTypes.getData);
// person types details
export const selectPersonTypesDetails = createSelector(selectAdminState, (state: AdminState) => state.personTypesDetailForm);
export const getPersonTypesDetails = createSelector(selectPersonTypesDetails, fromPersonTypesDetailForm.getFormState);
// address types list
export const selectAddressTypes = createSelector(selectAdminState, (state: AdminState) => state.addressTypes);
export const getAddressTypesLoaded = createSelector(selectAddressTypes, fromAddressTypes.getLoaded);
export const getAddressTypesLoading = createSelector(selectAddressTypes, fromAddressTypes.getLoading);
export const getAddressTypesFailed = createSelector(selectAddressTypes, fromAddressTypes.getFailed);
export const getAddressTypesData = createSelector(selectAddressTypes, fromAddressTypes.getData);
// address types details
export const selectAddressTypesDetails = createSelector(selectAdminState, (state: AdminState) => state.addressTypesDetailForm);
export const getAddressTypesDetails = createSelector(selectAddressTypesDetails, fromAddressTypesDetailForm.getFormState);

/**
 * External Persons communications data store functions
 */
export const selectExternalPersonCommunicationsData = createSelector(selectPersonState, (state: PersonState) => state.externalPersonDetailsCommunicationsData);
export const getExternalCommunicationsDataLoaded = createSelector(
  selectExternalPersonCommunicationsData,
  fromExternalPersonDetailsCommunicationsData.getLoaded
);
export const getExternalCommunicationsDataLoading = createSelector(
  selectExternalPersonCommunicationsData,
  fromExternalPersonDetailsCommunicationsData.getLoading
);
export const getExternalCommunicationsDataFailed = createSelector(
  selectExternalPersonCommunicationsData,
  fromExternalPersonDetailsCommunicationsData.getFailed
);
export const getExternalCommunicationsDataData = createSelector(selectExternalPersonCommunicationsData, fromExternalPersonDetailsCommunicationsData.getData);

/**
 * External Person CommunicationsData Details store functions
 */
export const selectExternalPersonCommunicationsDataDetails = createSelector(
  selectPersonState,
  (state: PersonState) => state.externalPersonDetailsCommunicationsDataDetailsForm
);
export const getExternalPersonCommunicationsDataDetails = createSelector(
  selectExternalPersonCommunicationsDataDetails,
  fromExternalPersonDetailsCommunicationsDataDetailsForm.getFormState
);

/**
 * Internal Persons communications data store functions
 */
export const selectInternalPersonCommunicationsData = createSelector(selectPersonState, (state: PersonState) => state.internalPersonDetailsCommunicationsData);
export const getInternalCommunicationsDataLoaded = createSelector(
  selectInternalPersonCommunicationsData,
  fromInternalPersonDetailsCommunicationsData.getLoaded
);
export const getInternalCommunicationsDataLoading = createSelector(
  selectInternalPersonCommunicationsData,
  fromInternalPersonDetailsCommunicationsData.getLoading
);
export const getInternalCommunicationsDataFailed = createSelector(
  selectInternalPersonCommunicationsData,
  fromInternalPersonDetailsCommunicationsData.getFailed
);
export const getInternalCommunicationsDataData = createSelector(selectInternalPersonCommunicationsData, fromInternalPersonDetailsCommunicationsData.getData);

/**
 * Internal Person CommunicationsData Details store functions
 */
export const selectInternalPersonCommunicationsDataDetails = createSelector(
  selectPersonState,
  (state: PersonState) => state.internalPersonDetailsCommunicationsDataDetailsForm
);
export const getInternalPersonCommunicationsDataDetails = createSelector(
  selectInternalPersonCommunicationsDataDetails,
  fromInternalPersonDetailsCommunicationsDataDetailsForm.getFormState
);

/**
 * Company store functions
 */
export const selectCompanyState = createFeatureSelector<CompanyState>('companyData');

export const selectCompanyDetails = createSelector(selectCompanyState, (state: CompanyState) => state.companyDetailsForm);
export const getCompanyDetails = createSelector(selectCompanyDetails, fromCompanyDetailForm.getFormState);

/**
 * Company communications data store functions
 */
export const selectCompanyCommunicationsData = createSelector(selectCompanyState, (state: CompanyState) => state.companyDetailsCommunicationsData);
export const getCompanyCommunicationsDataLoaded = createSelector(selectCompanyCommunicationsData, fromCompanyDetailsCommunicationsData.getLoaded);
export const getCompanyCommunicationsDataLoading = createSelector(selectCompanyCommunicationsData, fromCompanyDetailsCommunicationsData.getLoading);
export const getCompanyCommunicationsDataFailed = createSelector(selectCompanyCommunicationsData, fromCompanyDetailsCommunicationsData.getFailed);
export const getCompanyCommunicationsDataData = createSelector(selectCompanyCommunicationsData, fromCompanyDetailsCommunicationsData.getData);

/**
 * Company CommunicationsData Details store functions
 */
export const selectCompanyCommunicationsDataDetails = createSelector(
  selectCompanyState,
  (state: CompanyState) => state.companyDetailsCommunicationsDataDetailsForm
);
export const getCompanyCommunicationsDataDetails = createSelector(
  selectCompanyCommunicationsDataDetails,
  fromCompanyDetailsCommunicationsDataDetailsForm.getFormState
);
/**
 * Internal Persons Addresses store functions
 */
export const selectInternalPersonAddresses = createSelector(selectPersonState, (state: PersonState) => state.internalPersonDetailsAddresses);
export const getInternalAddressesLoaded = createSelector(selectInternalPersonAddresses, fromInternalPersonDetailsAddresses.getLoaded);
export const getInternalAddressesLoading = createSelector(selectInternalPersonAddresses, fromInternalPersonDetailsAddresses.getLoading);
export const getInternalAddressesFailed = createSelector(selectInternalPersonAddresses, fromInternalPersonDetailsAddresses.getFailed);
export const getInternalAddressesData = createSelector(selectInternalPersonAddresses, fromInternalPersonDetailsAddresses.getData);

/**
 * Internal Person Addresses Details store functions
 */
export const selectInternalPersonAddressesDetails = createSelector(selectPersonState, (state: PersonState) => state.internalPersonDetailsAddressesDetailsForm);
export const getInternalPersonAddressesDetails = createSelector(
  selectInternalPersonAddressesDetails,
  fromInternalPersonDetailsAddressesDetailsForm.getFormState
);

/**
 * Company Addresses store functions
 */
export const selectCompanyAddresses = createSelector(selectCompanyState, (state: CompanyState) => state.companyDetailsAddresses);
export const getCompanyAddressesLoaded = createSelector(selectCompanyAddresses, fromCompanyDetailsAddresses.getLoaded);
export const getCompanyAddressesLoading = createSelector(selectCompanyAddresses, fromCompanyDetailsAddresses.getLoading);
export const getCompanyAddressesFailed = createSelector(selectCompanyAddresses, fromCompanyDetailsAddresses.getFailed);
export const getCompanyAddressesData = createSelector(selectCompanyAddresses, fromCompanyDetailsAddresses.getData);

/**
 * Company Addresses Details store functions
 */
export const selectCompanyAddressesDetails = createSelector(selectCompanyState, (state: CompanyState) => state.companyDetailsAddressesDetailsForm);
export const getCompanyAddressesDetails = createSelector(selectCompanyAddressesDetails, fromCompanyDetailsAddressesDetailsForm.getFormState);

/**
 * Company Contact Persons store functions
 */
export const selectCompanyContactPersons = createSelector(selectCompanyState, (state: CompanyState) => state.companyDetailsContactPersons);
export const getCompanyContactPersonsLoaded = createSelector(selectCompanyContactPersons, fromCompanyDetailsContactPersons.getLoaded);
export const getCompanyContactPersonsLoading = createSelector(selectCompanyContactPersons, fromCompanyDetailsContactPersons.getLoading);
export const getCompanyContactPersonsFailed = createSelector(selectCompanyContactPersons, fromCompanyDetailsContactPersons.getFailed);
export const getCompanyContactPersonsData = createSelector(selectCompanyContactPersons, fromCompanyDetailsContactPersons.getData);

/**
 * Company Contact Person Details store functions
 */
export const selectCompanyContactPersonDetails = createSelector(selectCompanyState, (state: CompanyState) => state.companyDetailsContactPersonDetailsForm);
export const getCompanyContactPersonDetails = createSelector(selectCompanyContactPersonDetails, fromCompanyDetailsContactPersonsDetailForm.getFormState);

/**
 * Company Contact Person Addresses store functions
 */
export const selectContactPersonAddresses = createSelector(selectCompanyState, (state: CompanyState) => state.contactPersonDetailsAddresses);
export const getContactPersonAddressesLoaded = createSelector(selectContactPersonAddresses, fromContactPersonDetailsAddresses.getLoaded);
export const getContactPersonAddressesLoading = createSelector(selectContactPersonAddresses, fromContactPersonDetailsAddresses.getLoading);
export const getContactPersonAddressesFailed = createSelector(selectContactPersonAddresses, fromContactPersonDetailsAddresses.getFailed);
export const getContactPersonAddressesData = createSelector(selectContactPersonAddresses, fromContactPersonDetailsAddresses.getData);
/**
 * Company Contact Person Addresses Details store functions
 */
export const selectContactPersonAddressesDetails = createSelector(selectCompanyState, (state: CompanyState) => state.contactPersonDetailsAddressesDetailsForm);
export const getContactPersonAddressesDetails = createSelector(selectContactPersonAddressesDetails, fromContactPersonDetailsAddressesDetailsForm.getFormState);
/**
 * Company Contact Person Communications Data store functions
 */
export const selectContactPersonCommunicationsData = createSelector(selectCompanyState, (state: CompanyState) => state.contactPersonDetailsCommunicationsData);
export const getContactPersonCommunicationsDataLoaded = createSelector(
  selectContactPersonCommunicationsData,
  fromContactPersonDetailsCommunicationsData.getLoaded
);
export const getContactPersonCommunicationsDataLoading = createSelector(
  selectContactPersonCommunicationsData,
  fromContactPersonDetailsCommunicationsData.getLoading
);
export const getContactPersonCommunicationsDataFailed = createSelector(
  selectContactPersonCommunicationsData,
  fromContactPersonDetailsCommunicationsData.getFailed
);
export const getContactPersonCommunicationsDataData = createSelector(selectContactPersonCommunicationsData, fromContactPersonDetailsCommunicationsData.getData);

/**
 * Company Contact Person CommunicationsData Details store functions
 */
export const selectContactPersonCommunicationsDataDetails = createSelector(
  selectCompanyState,
  (state: CompanyState) => state.contactPersonDetailsCommunicationsDataDetailsForm
);
export const getContactPersonCommunicationsDataDetails = createSelector(
  selectContactPersonCommunicationsDataDetails,
  fromContactPersonDetailsCommunicationsDataDetailsForm.getFormState
);
/**
 * UserModuleAssignment store functions
 */
export const selectUserModuleAssignmentState = createFeatureSelector<UserModuleAssignmentState>('userModuleAssignmentData');

export const selectUserModuleAssignments = createSelector(selectUserModuleAssignmentState, (state: UserModuleAssignmentState) => state.userModuleAssignments);
export const getUserModuleAssignmentsLoaded = createSelector(selectUserModuleAssignments, fromUserModuleAssignments.getLoaded);
export const getUserModuleAssignmentsLoading = createSelector(selectUserModuleAssignments, fromUserModuleAssignments.getLoading);
export const getUserModuleAssignmentsFailed = createSelector(selectUserModuleAssignments, fromUserModuleAssignments.getFailed);
export const getUserModuleAssignmentsData = createSelector(selectUserModuleAssignments, fromUserModuleAssignments.getData);

/**
 * User Module Assignment Details store functions
 */
export const selectUserModuleAssignmentDetails = createSelector(
  selectUserModuleAssignmentState,
  (state: UserModuleAssignmentState) => state.userModuleAssignmentDetailsForm
);
export const getUserModuleAssignmentDetails = createSelector(selectUserModuleAssignmentDetails, fromUserModuleAssignmentDetailsForm.getFormState);

// user module types list
export const selectUserModuleTypes = createSelector(selectUserModuleAssignmentState, (state: UserModuleAssignmentState) => state.userModuleTypes);
export const getUserModuleTypesLoaded = createSelector(selectUserModuleTypes, fromUserModuleTypes.getLoaded);
export const getUserModuleTypesLoading = createSelector(selectUserModuleTypes, fromUserModuleTypes.getLoading);
export const getUserModuleTypesFailed = createSelector(selectUserModuleTypes, fromUserModuleTypes.getFailed);
export const getUserModuleTypesData = createSelector(selectUserModuleTypes, fromUserModuleTypes.getData);

/**
 * Keycloak users store functions
 */
export const selectKeycloakUsers = createSelector(selectPersonState, (state: PersonState) => state.keycloakUsers);
export const getKeycloakUsersLoaded = createSelector(selectKeycloakUsers, fromKeycloakUser.getLoaded);
export const getKeycloakUsersLoading = createSelector(selectKeycloakUsers, fromKeycloakUser.getLoading);
export const getKeycloakUsersFailed = createSelector(selectKeycloakUsers, fromKeycloakUser.getFailed);
export const getKeycloakUsersData = createSelector(selectKeycloakUsers, fromKeycloakUser.getData);

/**
 * Ldap users store functions
 */
export const selectLdapUsers = createSelector(selectPersonState, (state: PersonState) => state.ldapUsers);
export const getLdapUsersLoaded = createSelector(selectLdapUsers, fromLdapUser.getLoaded);
export const getLdapUsersLoading = createSelector(selectLdapUsers, fromLdapUser.getLoading);
export const getLdapUsersFailed = createSelector(selectLdapUsers, fromLdapUser.getFailed);
export const getLdapUsersData = createSelector(selectLdapUsers, fromLdapUser.getData);
