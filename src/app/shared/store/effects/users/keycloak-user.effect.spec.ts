/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PersonsApiClient } from '@pages/persons/persons-api-client';
import { Subject, of, throwError } from 'rxjs';
import * as keycloakUserActions from '@shared/store/actions/users/keycloak-user.action';
import { KeycloakUser } from '@shared/models';
import { KeycloakUserEffect } from '@shared/store/effects/users/keycloak-user.effect';

describe('KeycloakUserEffect Effects', () => {
  let effects: KeycloakUserEffect;
  let actions$: Subject<any>;
  let apiClient: PersonsApiClient;
  let apiResponse: any = null;

  beforeEach(() => {
    apiClient = {
      getKeycloakUsers() {},
    } as any;

    actions$ = new Subject();

    effects = new KeycloakUserEffect(actions$, apiClient);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadKeycloakUsersSuccess after loadKeycloakUsers', done => {
    apiResponse = [new KeycloakUser()];
    spyOn(apiClient, 'getKeycloakUsers').and.returnValue(of(apiResponse));
    effects.getKeycloakUsers$.pipe().subscribe(result => {
      expect(result).toEqual(keycloakUserActions.loadKeycloakUsersSuccess({ payload: [new KeycloakUser()] }));
    });
    done();
    actions$.next(keycloakUserActions.loadKeycloakUsers());
  });

  it('should equal loadKeycloakUsersFail after loadKeycloakUsers', done => {
    spyOn(apiClient, 'getKeycloakUsers').and.returnValue(throwError('error'));
    effects.getKeycloakUsers$.pipe().subscribe(result => {
      expect(result).toEqual(keycloakUserActions.loadKeycloakUsersFail({ payload: 'error' }));
    });
    done();
    actions$.next(keycloakUserActions.loadKeycloakUsers());
  });
});
