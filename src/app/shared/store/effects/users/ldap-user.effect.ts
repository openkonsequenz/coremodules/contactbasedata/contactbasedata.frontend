/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { PersonsApiClient } from '@pages/persons/persons-api-client';
import * as ldapUserActions from '@shared/store/actions/users/ldap-user.action';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class LdapUserEffect {
  getLdapUsers$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(ldapUserActions.loadLdapUsers),
      switchMap(() => {
        return this._personsApiClient.getLdapUsers().pipe(
          map(users => ldapUserActions.loadLdapUsersSuccess({ payload: users })),
          catchError(error => of(ldapUserActions.loadLdapUsersFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _personsApiClient: PersonsApiClient) {}
}
