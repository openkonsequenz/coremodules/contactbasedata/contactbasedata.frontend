/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { of } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import * as companyActions from '@shared/store/actions/company/company.action';
import * as contactPersonActions from '@shared/store/actions/company/contact-person.actions';
import { CompanyApiClient } from '@pages/company/company-api-client';
import { ContactPerson, CommunicationsData, Address } from '@shared/models';
import { Store } from '@ngrx/store';
import { State } from '@shared/store';

@Injectable()
export class ContactPersonEffect {
  /**
   * load contact person details
   */
  getContactPersonsDetails$ = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.loadContactPersonDetail),
      switchMap(action => {
        return this._companyApiClient.getContactPersonsDetails(action['payload']).pipe(
          map((contactPerson: ContactPerson) => contactPersonActions.loadContactPersonDetailSuccess({ payload: contactPerson })),
          catchError(error => of(contactPersonActions.loadContactPersonDetailFail({ payload: error })))
        );
      })
    )
  );
  /**
   * saves new or edited contact person
   */
  saveContactPerson$ = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.saveContactPersonDetail),
      map(action => action['payload']),
      switchMap((personDetails: ContactPerson) => {
        return (
          personDetails.contactId
            ? this._companyApiClient.putContactPersonDetails(personDetails.contactId, personDetails)
            : this._companyApiClient.postContactPersonDetails(personDetails)
        ).pipe(
          map((contactPerson: ContactPerson) => {
            return contactPersonActions.saveContactPersonDetailSuccess({ payload: contactPerson });
          }),
          catchError(error => of(contactPersonActions.saveContactPersonDetailFail({ payload: error })))
        );
      })
    )
  );
  /**
   * deletes contact person
   */
  deleteContactPerson$ = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.deleteContactPerson),
      map(action => action['payload']),
      switchMap((person: ContactPerson) => {
        return this._companyApiClient.deleteContactPersonDetails(person.contactId).pipe(
          map(() => {
            this._store.dispatch(companyActions.loadCompanyDetailContactPersons({ payload: person.companyContactId }));
            return contactPersonActions.deleteContactPersonSuccess();
          }),
          catchError(error => of(contactPersonActions.deleteContactPersonFail({ payload: error })))
        );
      })
    )
  );
  /**
   * load contact persons addresses
   */
  getContactPersonAddresses$ = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.loadContactPersonDetailAddresses),
      switchMap((action: any) => {
        return this._companyApiClient.getAddresses(action['payload']).pipe(
          map((addresses: Array<Address>) => contactPersonActions.loadContactPersonDetailAddressesSuccess({ payload: addresses })),
          catchError(error => of(contactPersonActions.loadContactPersonDetailAddressesFail({ payload: error })))
        );
      })
    )
  );
  /**
   * load contact person address details
   */
  getContactPersonAddressDetails$ = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.loadContactPersonDetailAddressDetails),
      switchMap(action => {
        return this._companyApiClient.getAddressesDetails(action.payload_contactId, action.payload_addressId).pipe(
          map((addressDetails: Address) => contactPersonActions.loadContactPersonDetailAddressDetailsSuccess({ payload: addressDetails })),
          catchError(error => of(contactPersonActions.loadContactPersonDetailAddressDetailsFail({ payload: error })))
        );
      })
    )
  );
  /**
   * persist new or edited contact person address
   */
  persistAddress$ = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.persistAddressDetail),
      map(action => action['payload']),
      switchMap((address: Address) => {
        return (
          address.id
            ? this._companyApiClient.putAddressDetails(address.contactId, address.id, address)
            : this._companyApiClient.postAddressDetails(address.contactId, address)
        ).pipe(
          map(() => {
            this._store.dispatch(contactPersonActions.loadContactPersonDetailAddresses({ payload: address.contactId }));
            return contactPersonActions.persistAddressDetailSuccess({ payload: address });
          }),
          catchError(error => of(contactPersonActions.persistAddressDetailFail({ payload: error })))
        );
      })
    )
  );
  /**
   * delete contact person address
   */
  deleteAddress$ = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.deleteAddress),
      map(action => action['payload']),
      switchMap((address: Address) => {
        return this._companyApiClient.deleteAddress(address.contactId, address.id).pipe(
          map(() => {
            this._store.dispatch(contactPersonActions.loadContactPersonDetailAddresses({ payload: address.contactId }));
            return contactPersonActions.deleteAddressSuccess();
          }),
          catchError(error => of(contactPersonActions.deleteAddressFail({ payload: error })))
        );
      })
    )
  );

  /**
   * load contact person details communications data
   */
  getContactPersonCommunicationsData$ = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.loadContactPersonDetailCommunicationsData),
      switchMap((action: any) => {
        return this._companyApiClient.getCommunicationsData(action['payload']).pipe(
          map((communicationsData: Array<CommunicationsData>) =>
            contactPersonActions.loadContactPersonDetailCommunicationsDataSuccess({ payload: communicationsData })
          ),
          catchError(error => of(contactPersonActions.loadContactPersonDetailCommunicationsDataFail({ payload: error })))
        );
      })
    )
  );

  /**
   * load contact person communications data details
   */
  getCompanyCommunicationsDataDetails$ = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.loadContactPersonDetailCommunicationsDataDetails),
      switchMap(action => {
        return this._companyApiClient.getCommunicationsDataDetails(action.payload_contactId, action.payload_communicationsId).pipe(
          map((communicationsDataDetails: CommunicationsData) =>
            contactPersonActions.loadContactPersonDetailCommunicationsDataDetailsSuccess({ payload: communicationsDataDetails })
          ),
          catchError(error => of(contactPersonActions.loadContactPersonDetailCommunicationsDataDetailsFail({ payload: error })))
        );
      })
    )
  );

  /**
   * persist new or edited contact person communications data
   */
  persistCommunicationsData$ = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.persistCommunicationsDataDetail),
      map(action => action['payload']),
      switchMap((data: CommunicationsData) => {
        return (
          data.id
            ? this._companyApiClient.putCommunicationsDataDetails(data.contactId, data.id, data)
            : this._companyApiClient.postCommunicationsDataDetails(data.contactId, data)
        ).pipe(
          map(() => {
            this._store.dispatch(contactPersonActions.loadContactPersonDetailCommunicationsData({ payload: data.contactId }));
            return contactPersonActions.persistCommunicationsDataDetailSuccess({ payload: data });
          }),
          catchError(error => of(contactPersonActions.persistCommunicationsDataDetailFail({ payload: error })))
        );
      })
    )
  );

  /**
   * delete contact person communications data
   */
  deleteCommunicationsData$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(contactPersonActions.deleteCommunicationsData),
      map(action => action['payload']),
      switchMap((address: CommunicationsData) => {
        return this._companyApiClient.deleteCommunicationsData(address.contactId, address.id).pipe(
          map(() => {
            this._store.dispatch(contactPersonActions.loadContactPersonDetailCommunicationsData({ payload: address.contactId }));
            return contactPersonActions.deleteCommunicationsDataSuccess();
          }),
          catchError(error => of(contactPersonActions.deleteCommunicationsDataFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _companyApiClient: CompanyApiClient, private _store: Store<State>) {}
}
