/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import * as addressTypesActions from '@shared/store/actions/admin/address-types.action';
import { AddressTypesApiClient } from '@pages/admin/address-types/address-types-api-client';
import { catchError, map, switchMap } from 'rxjs/operators';
import { AddressType } from '@shared/models';
import { Store } from '@ngrx/store';

@Injectable()
export class AddressTypesEffects {
  getAddressTypes$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(addressTypesActions.loadAddressTypes),
      switchMap(() => {
        return this._addressTypesApiClient.getAddressTypes().pipe(
          map(item => addressTypesActions.loadAddressTypesSuccess({ payload: item })),
          catchError(error => of(addressTypesActions.loadAddressTypesFail({ payload: error })))
        );
      })
    )
  );

  getAddressType$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(addressTypesActions.loadAddressType),
      switchMap(action => {
        return this._addressTypesApiClient.getAddressTypeDetails(action['payload']).pipe(
          map((item: AddressType) => addressTypesActions.loadAddressTypeSuccess({ payload: item })),
          catchError(error => of(addressTypesActions.loadAddressTypeFail({ payload: error })))
        );
      })
    )
  );

  saveAddressType$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(addressTypesActions.saveAddressType),
      map(action => action['payload']),
      switchMap((payload: AddressType) => {
        return (payload.id ? this._addressTypesApiClient.putAddressType(payload.id, payload) : this._addressTypesApiClient.postAddressType(payload)).pipe(
          map((item: AddressType) => {
            this._store.dispatch(addressTypesActions.loadAddressTypes());
            return addressTypesActions.saveAddressTypeSuccess({ payload: item });
          }),
          catchError(error => of(addressTypesActions.saveAddressTypeFail({ payload: error })))
        );
      })
    )
  );

  deleteAddressType$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(addressTypesActions.deleteAddressType),
      map(action => action['payload']),
      switchMap((id: string) => {
        return this._addressTypesApiClient.deleteAddressType(id).pipe(
          map(() => {
            this._store.dispatch(addressTypesActions.loadAddressTypes());
            return addressTypesActions.deleteAddressTypeSuccess();
          }),
          catchError(error => of(addressTypesActions.deleteAddressTypeFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _addressTypesApiClient: AddressTypesApiClient, private _store: Store<any>) {}
}
