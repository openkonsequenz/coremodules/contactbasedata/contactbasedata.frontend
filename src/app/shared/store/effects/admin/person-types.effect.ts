/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import * as personTypesActions from '@app/shared/store/actions/admin/person-types.action';
import { PersonTypesApiClient } from '@pages/admin/person-types/person-types-api-client';
import { catchError, map, switchMap } from 'rxjs/operators';
import { PersonType } from '@shared/models';
import { Store } from '@ngrx/store';

@Injectable()
export class PersonTypesEffects {
  getPersonTypes$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(personTypesActions.loadPersonTypes),
      switchMap(() => {
        return this._personTypesApiClient.getPersonTypes().pipe(
          map(item => personTypesActions.loadPersonTypesSuccess({ payload: item })),
          catchError(error => of(personTypesActions.loadPersonTypesFail({ payload: error })))
        );
      })
    )
  );

  getPersonType$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(personTypesActions.loadPersonType),
      switchMap(action => {
        return this._personTypesApiClient.getPersonTypeDetails(action['payload']).pipe(
          map((item: PersonType) => personTypesActions.loadPersonTypeSuccess({ payload: item })),
          catchError(error => of(personTypesActions.loadPersonTypeFail({ payload: error })))
        );
      })
    )
  );

  savePersonType$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(personTypesActions.savePersonType),
      map(action => action['payload']),
      switchMap((payload: PersonType) => {
        return (payload.id ? this._personTypesApiClient.putPersonType(payload.id, payload) : this._personTypesApiClient.postPersonType(payload)).pipe(
          map((item: PersonType) => {
            this._store.dispatch(personTypesActions.loadPersonTypes());
            return personTypesActions.savePersonTypeSuccess({ payload: item });
          }),
          catchError(error => of(personTypesActions.savePersonTypeFail({ payload: error })))
        );
      })
    )
  );

  deletePersonType$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(personTypesActions.deletePersonType),
      map(action => action['payload']),
      switchMap((id: string) => {
        return this._personTypesApiClient.deletePersonType(id).pipe(
          map(() => {
            this._store.dispatch(personTypesActions.loadPersonTypes());
            return personTypesActions.deletePersonTypeSuccess();
          }),
          catchError(error => of(personTypesActions.deletePersonTypeFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _personTypesApiClient: PersonTypesApiClient, private _store: Store<any>) {}
}
