/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { take } from 'rxjs/operators';
import { CommunicationTypesEffects } from '@shared/store/effects/admin/communication-types.effect';
import { Subject, of, throwError } from 'rxjs';
import { CommunicationType } from '@shared/models';
import * as communicationTypesActions from '@shared/store/actions/admin/communication-types.action';
import { CommunicationTypesApiClient } from '@pages/admin/communication-types/communication-types-api-client';
import { Store } from '@ngrx/store';

describe('CommunicationTypes Effects', () => {

  let effects: CommunicationTypesEffects;
  let actions$: Subject<any>;
  let apiClient: CommunicationTypesApiClient;
  let store: Store<any>;
  let apiResponse: any = null;

  beforeEach(() => {
    apiClient = {
      getCommunicationTypes() {},
      getCommunicationType() {},
      putCommunicationType() {},
      postCommunicationType() {},
      deleteCommunicationType() {}
    } as any;
    store = {
      dispatch() {}
    } as any;
    actions$ = new Subject();
    effects = new CommunicationTypesEffects(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadCommunicationTypesSuccess after getCommunicationTypes', (done) => {
    apiResponse =  [new CommunicationType({id: "1"})];
    spyOn(apiClient, 'getCommunicationTypes').and.returnValue(of(apiResponse));
    effects.getCommunicationTypes$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(communicationTypesActions.loadCommunicationTypesSuccess({payload: apiResponse}));
    });
    done();
    actions$.next(communicationTypesActions.loadCommunicationTypes());
  });

  it('should equal loadCommunicationTypesFail after getCommunicationTypes Error', (done) => {
    spyOn(apiClient, 'getCommunicationTypes').and.returnValue(throwError('x'));
    effects.getCommunicationTypes$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(communicationTypesActions.loadCommunicationTypesFail({payload: 'x'}));
    });
    done();
    actions$.next(communicationTypesActions.loadCommunicationTypes());
  });

  it('should equal loadCommunicationTypeSuccess after getCommunicationType', (done) => {
    apiResponse =  new CommunicationType({id: "1"});
    spyOn(apiClient, 'getCommunicationType').and.returnValue(of(apiResponse));
    effects.getCommunicationType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(communicationTypesActions.loadCommunicationTypeSuccess({payload: apiResponse}));
    });
    done();
    actions$.next(communicationTypesActions.loadCommunicationType({payload: "1"}));
  });

  it('should equal loadCommunicationTypeFail after getCommunicationType Error', (done) => {
    spyOn(apiClient, 'getCommunicationType').and.returnValue(throwError('x'));
    effects.getCommunicationType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(communicationTypesActions.loadCommunicationTypeFail({payload: 'x'}));
    });
    done();
    actions$.next(communicationTypesActions.loadCommunicationType({payload: "1"}));
  });

  it('should equal saveCommunicationTypeSuccess after saveCommunicationType', (done) => {
    apiResponse =  new CommunicationType({id: "1"});
    spyOn(apiClient, 'putCommunicationType').and.returnValue(of(apiResponse));
    effects.saveCommunicationType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(communicationTypesActions.saveCommunicationTypeSuccess({payload: new CommunicationType({id: "1"})}));
    });
    done();
    actions$.next(communicationTypesActions.saveCommunicationType({payload: new CommunicationType({id: "1"})}));
  });

  it('should equal saveCommunicationTypeFail after saveCommunicationType Error', (done) => {
    spyOn(apiClient, 'putCommunicationType').and.returnValue(throwError('x'));
    effects.saveCommunicationType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(communicationTypesActions.saveCommunicationTypeFail({payload: 'x'}));
    });
    done();
    actions$.next(communicationTypesActions.saveCommunicationType({payload: new CommunicationType({id: "1"})}));
  });

  it('should equal deleteCommunicationTypeSuccess after deleteCommunicationType', (done) => {
    apiResponse =  new CommunicationType({id: "1"});
    spyOn(apiClient, 'deleteCommunicationType').and.returnValue(of(null));
    effects.deleteCommunicationType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(communicationTypesActions.deleteCommunicationTypeSuccess());
    });
    done();
    actions$.next(communicationTypesActions.deleteCommunicationType({payload: "1"}));
  });

});
