 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PersonsApiClient } from '@pages/persons/persons-api-client';
import { ExternalPersonEffect } from "@shared/store/effects/persons/external-person.effect";
import { Subject, of, throwError } from 'rxjs';
import * as externalPersonActions from '@shared/store/actions/persons/external-person.action';
import { ExternalPerson, Address, CommunicationsData } from '@shared/models';

describe('ExternalPerson Effects', () => {
  let effects: ExternalPersonEffect;
  let actions$: Subject<any>;
  let apiClient: PersonsApiClient;
  let apiResponse: any = null;
  let store: any;

  beforeEach(() => {
    apiClient = {
      getExternalPersonDetails() {},
      putExternalPersonDetails: () => {},
      postExternalPersonDetails: () => {},
      putAddressDetails: () => {},
      postAddressDetails: () => {},
      deleteAddress: () => {},
      putCommunicationsDataDetails: () => {},
      postCommunicationsDataDetails: () => {},
      deleteCommunicationsData: () => {},
      getAddressesDetails: () => {},
      getAddresses: () => {},
    } as any;

    actions$ = new Subject();

    store = {
      dispatch() {}
    }

    effects = new ExternalPersonEffect(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadExternalPersonDetail after getExternalPerson', (done) => {
    apiResponse =  new ExternalPerson({id: "1"});
    spyOn(apiClient, 'getExternalPersonDetails').and.returnValue(of(apiResponse));
    effects.getExternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.loadExternalPersonDetailSuccess({payload: new ExternalPerson({id: "1"})}));
    });
    done();
    actions$.next(externalPersonActions.loadExternalPersonDetail({payload: "1"}));
  });

  it('should equal loadExternalPersonDetailFail after getExternalPerson', (done) => {
    spyOn(apiClient, 'getExternalPersonDetails').and.returnValue(throwError('error'));
    effects.getExternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.loadExternalPersonDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.loadExternalPersonDetail({payload: "1"}));
  });

  it('should equal putExternalPersonDetails after persistExternalPerson with given contactId', (done) => {
    apiResponse =  new ExternalPerson({contactId: "1"});
    spyOn(apiClient, 'putExternalPersonDetails').and.returnValue(of(apiResponse));
    effects.persistExternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistExternalPersonDetailSuccess({payload: new ExternalPerson({contactId: "1"})}));
    });
    done();
    actions$.next(externalPersonActions.persistExternalPersonDetail({payload: new ExternalPerson({contactId: "1"})}));
  });

  it('should equal postExternalPersonDetails after persistExternalPerson a new external person', (done) => {
    apiResponse =  new ExternalPerson();
    spyOn(apiClient, 'postExternalPersonDetails').and.returnValue(of(apiResponse));
    effects.persistExternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistExternalPersonDetailSuccess({payload: new ExternalPerson()}));
    });
    done();
    actions$.next(externalPersonActions.persistExternalPersonDetail({payload: new ExternalPerson()}));
  });

  it('should equal persistExternalPersonDetailFail after putExternalPersonDetails', (done) => {
    spyOn(apiClient, 'putExternalPersonDetails').and.returnValue(throwError('error'));
    effects.persistExternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistExternalPersonDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.persistExternalPersonDetail({payload: new ExternalPerson({contactId: "1"})}));
  });

  it('should equal persistExternalPersonDetailFail after postExternalPersonDetails', (done) => {
    spyOn(apiClient, 'postExternalPersonDetails').and.returnValue(throwError('error'));
    effects.persistExternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistExternalPersonDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.persistExternalPersonDetail({payload: new ExternalPerson()}));
  });

  it('should equal loadAddressDetail after getAddresses', (done) => {
    apiResponse =  [new Address({contactId: "1"})];
    spyOn(apiClient, 'getAddresses').and.returnValue(of(apiResponse));
    effects.getExternalPersonAddresses$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.loadExternalPersonDetailAddressesSuccess({payload: [new Address({contactId: "1"})]}));
    });
    done();
    actions$.next(externalPersonActions.loadExternalPersonDetailAddresses({payload: "1"}));
  });

  it('should equal loadExternalPersonDetailFail after getAddresses', (done) => {
    spyOn(apiClient, 'getAddresses').and.returnValue(throwError('error'));
    effects.getExternalPersonAddresses$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.loadExternalPersonDetailAddressesFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.loadExternalPersonDetailAddresses({payload: "1"}));
  });

  it('should equal loadAddressDetail after getAddressesDetails', (done) => {
    apiResponse =  new Address({contactId: "1", id: "1"});
    spyOn(apiClient, 'getAddressesDetails').and.returnValue(of(apiResponse));
    effects.getExternalPersonAddressDetails$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.loadExternalPersonDetailAddressDetailsSuccess({payload: new Address({contactId: "1", id: "1"})}));
    });
    done();
    actions$.next(externalPersonActions.loadExternalPersonDetailAddressDetails({payload_contactId: "1", payload_addressId: "1"}));
  });

  it('should equal loadExternalPersonDetailFail after getAddressesDetails', (done) => {
    spyOn(apiClient, 'getAddressesDetails').and.returnValue(throwError('error'));
    effects.getExternalPersonAddressDetails$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.loadExternalPersonDetailAddressDetailsFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.loadExternalPersonDetailAddressDetails({payload_contactId: "1", payload_addressId: "1"}));
  });

  it('should equal persistAddressDetailSuccess after persistAddressDetail with given contactId', (done) => {
    apiResponse =  new Address({contactId: "1", id: "1"});
    spyOn(apiClient, 'putAddressDetails').and.returnValue(of(apiResponse));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistAddressDetailSuccess({payload: new Address({contactId: "1", id: "1"})}));
    });
    done();
    actions$.next(externalPersonActions.persistAddressDetail({payload: new Address({contactId: "1", id: "1"})}));
  });

  it('should equal persistAddressDetailSuccess after persistAddressDetail a new external person', (done) => {
    apiResponse =  new Address();
    spyOn(apiClient, 'postAddressDetails').and.returnValue(of(apiResponse));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistAddressDetailSuccess({payload: new Address()}));
    });
    done();
    actions$.next(externalPersonActions.persistAddressDetail({payload: new Address()}));
  });

  it('should equal persistAddressDetailFail after putAddressDetails', (done) => {
    spyOn(apiClient, 'putAddressDetails').and.returnValue(throwError('error'));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistAddressDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.persistAddressDetail({payload: new Address({contactId: "1", id: "1"})}));
  });

  it('should equal persistAddressDetailFail after postAddressDetails', (done) => {
    spyOn(apiClient, 'postAddressDetails').and.returnValue(throwError('error'));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistAddressDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.persistAddressDetail({payload: new Address ()}));
  });

  it('should equal deleteAddressSuccess after deleteAddress', (done) => {
    apiResponse =  new Address({contactId: "1", id: "1"});
    spyOn(apiClient, 'deleteAddress').and.returnValue(of(apiResponse));
    effects.deleteAddress$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.deleteAddressSuccess());
    });
    done();
    actions$.next(externalPersonActions.deleteAddress({payload: new Address ({contactId: "1", id: "1"})}));
  });

  it('should equal deleteAddressFail after deleteAddress', (done) => {
    spyOn(apiClient, 'deleteAddress').and.returnValue(throwError('error'));
    effects.deleteAddress$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.deleteAddressFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.deleteAddress({payload: new Address ({contactId: "1", id: "1"})}));
  });

  it('should equal persistCommunicationsDataDetailSuccess after persistCommunicationsDataDetail with given contactId', (done) => {
    apiResponse =  new CommunicationsData({contactId: "1", id: "1"});
    spyOn(apiClient, 'putCommunicationsDataDetails').and.returnValue(of(apiResponse));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistCommunicationsDataDetailSuccess({payload: new CommunicationsData({contactId: "1", id: "1"})}));
    });
    done();
    actions$.next(externalPersonActions.persistCommunicationsDataDetail({payload: new CommunicationsData({contactId: "1", id: "1"})}));
  });

  it('should equal persistCommunicationsDataDetailSuccess after persistCommunicationsDataDetail a new external person', (done) => {
    apiResponse =  new CommunicationsData();
    spyOn(apiClient, 'postCommunicationsDataDetails').and.returnValue(of(apiResponse));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistCommunicationsDataDetailSuccess({payload: new CommunicationsData()}));
    });
    done();
    actions$.next(externalPersonActions.persistCommunicationsDataDetail({payload: new CommunicationsData()}));
  });

  it('should equal persistCommunicationsDataDetailFail after putCommunicationsDataDetails', (done) => {
    spyOn(apiClient, 'putCommunicationsDataDetails').and.returnValue(throwError('error'));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistCommunicationsDataDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.persistCommunicationsDataDetail({payload: new CommunicationsData({contactId: "1", id: "1"})}));
  });

  it('should equal persistCommunicationsDataDetailFail after postCommunicationsDataDetails', (done) => {
    spyOn(apiClient, 'postCommunicationsDataDetails').and.returnValue(throwError('error'));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.persistCommunicationsDataDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.persistCommunicationsDataDetail({payload: new CommunicationsData ()}));
  });

  it('should equal deleteCommunicationsDataSuccess after deleteCommunicationsData', (done) => {
    apiResponse =  new CommunicationsData({contactId: "1", id: "1"});
    spyOn(apiClient, 'deleteCommunicationsData').and.returnValue(of(apiResponse));
    effects.deleteCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.deleteCommunicationsDataSuccess());
    });
    done();
    actions$.next(externalPersonActions.deleteCommunicationsData({payload: new CommunicationsData ({contactId: "1", id: "1"})}));
  });

  it('should equal deleteCommunicationsDataFail after deleteCommunicationsData', (done) => {
    spyOn(apiClient, 'deleteCommunicationsData').and.returnValue(throwError('error'));
    effects.deleteCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(externalPersonActions.deleteCommunicationsDataFail({payload: 'error'}));
    });
    done();
    actions$.next(externalPersonActions.deleteCommunicationsData({payload: new CommunicationsData ({contactId: "1", id: "1"})}));
  });
});
