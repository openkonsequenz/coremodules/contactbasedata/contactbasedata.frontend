 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as externalPersonActions from '@shared/store/actions/persons/external-person.action';
import {
  createFormGroupState,
  createFormStateReducerWithUpdate,
  updateGroup,
  FormGroupState,
  FormState,
  SetValueAction,
  validate,
} from 'ngrx-forms';
import { ExternalPerson } from '@shared/models';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'externalPersonDetailsForm';

export const INITIAL_STATE: FormGroupState<ExternalPerson> = createFormGroupState<ExternalPerson>(FORM_ID, new ExternalPerson());

export const validateForm: ActionReducer<FormState<ExternalPerson>> = createFormStateReducerWithUpdate<ExternalPerson>(
  updateGroup<ExternalPerson>(
    {
      lastName: validate(required),
    }
  )
);

export function reducer(
  state: FormGroupState<ExternalPerson> = INITIAL_STATE,
  action: Action
): FormGroupState<ExternalPerson> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }

  if (action.type === externalPersonActions.loadExternalPersonDetailSuccess.type) {
    const externalPerson: ExternalPerson = <ExternalPerson>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, externalPerson);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}

export const getFormState = (state: FormGroupState<ExternalPerson>) => state;
