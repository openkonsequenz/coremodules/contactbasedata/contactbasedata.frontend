/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as internalPersonActions from '@shared/store/actions/persons/internal-person.action';
import { createFormGroupState, createFormStateReducerWithUpdate, updateGroup, FormGroupState, FormState, SetValueAction, validate } from 'ngrx-forms';
import { InternalPerson } from '@shared/models';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'internalPersonDetailsForm';

export const INITIAL_STATE: FormGroupState<InternalPerson> = createFormGroupState<InternalPerson>(FORM_ID, new InternalPerson());

export const validateForm: ActionReducer<FormState<InternalPerson>> = createFormStateReducerWithUpdate<InternalPerson>(
  updateGroup<InternalPerson>({
    lastName: validate(required),
  })
);

export function reducer(state: FormGroupState<InternalPerson> = INITIAL_STATE, action: Action): FormGroupState<InternalPerson> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }

  if (action.type === internalPersonActions.loadInternalPersonDetailSuccess.type) {
    const internalPerson: InternalPerson = <InternalPerson>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, internalPerson);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}

export const getFormState = (state: FormGroupState<InternalPerson>) => state;
