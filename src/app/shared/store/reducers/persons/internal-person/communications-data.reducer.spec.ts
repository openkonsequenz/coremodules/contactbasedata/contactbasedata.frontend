 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {internalPersonCommunicationsDataReducer, INITIAL_STATE, getData, getLoading, getLoaded, getFailed, reducer} from '@shared/store/reducers/persons/internal-person/communications-data.reducer';
import * as addressActions from '@shared/store/actions/persons/internal-person.action';
import { CommunicationsData } from '@shared/models';

describe('InternalPersonCommunicationsData reducer', () => {

  it('should return the initial state', () => {
    const action = { type: 'NOOP' } as any;
    const result = reducer(undefined, action);

    expect(result).toBe(INITIAL_STATE);
  });

  it('should trigger loading state', () => {
    const action = addressActions.loadInternalPersonDetailCommunicationsData({payload: 'id'});
    const result = internalPersonCommunicationsDataReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      loading: true,
    });
  });

  it('should trigger loaded state', () => {
    const addresses: addressActions.ILoadInternalPersonCommunicationsDataSuccess = {payload: [new CommunicationsData()]};
    addresses.payload[0].communicationTypeId = 'testme';
    const action = addressActions.loadInternalPersonDetailCommunicationsDataSuccess(addresses);
    const result = internalPersonCommunicationsDataReducer(INITIAL_STATE, action);

    expect(result.loaded).toBe(true);
  });

  it('should trigger failed state', () => {
    const error: addressActions.ILoadInternalPersonCommunicationsDataFail = {payload: 'err_msg'};
    const action = addressActions.loadInternalPersonDetailCommunicationsDataFail(error);
    const result = internalPersonCommunicationsDataReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      failed: true,
    });
  });

  it('getData return state.data', () => {
    const state = { ...INITIAL_STATE };
    const result = getData(state);
    expect(result).toBe(state.data);
  });

  it('getLoading return state.loading', () => {
    const state = { ...INITIAL_STATE };
    const result = getLoading(state);
    expect(result).toBe(state.loading);
  });

  it('getLoaded return state.loaded', () => {
    const state = { ...INITIAL_STATE };
    const result = getLoaded(state);
    expect(result).toBe(state.loaded);
  });

  it('getFailed return state.failed', () => {
    const state = { ...INITIAL_STATE };
    const result = getFailed(state);
    expect(result).toBe(state.failed);
  });

});
