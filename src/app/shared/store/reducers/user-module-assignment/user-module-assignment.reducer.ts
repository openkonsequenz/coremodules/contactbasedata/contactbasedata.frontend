 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { UserModuleAssignment } from '@shared/models';
import * as userModuleAssignmentActions from '@shared/store/actions/user-module-assignment/user-module-assignment.action'
import { createReducer, on } from '@ngrx/store';

export interface State {
  loading: boolean;
  loaded: boolean;
  failed: boolean;
  data: Array<UserModuleAssignment>;
}

export const INITIAL_STATE: State = {
  loading: false,
  loaded: false,
  failed: false,
  data: []
};
export const userModuleAssignmentReducer = createReducer(
  INITIAL_STATE,
  on(userModuleAssignmentActions.loadUserModuleAssignments, (state: any, action: any) => {
    return {
      ...state,
      loading: true,
      loaded: false,
      failed: false,
      data: []
    };
  }),
  on(userModuleAssignmentActions.loadUserModuleAssignmentsSuccess, (state: any, action: any) => {
    return {
      ...state,
      loading: false,
      loaded: true,
      failed: false,
      data: action['payload']
    };
  }),
  on(userModuleAssignmentActions.loadUserModuleAssignmentsFail, (state: any, action: any) => {
    return {
      ...state,
      loaded: false,
      loading: false,
      failed: true,
      data: []
    };
  })
);
export function reducer(state = INITIAL_STATE, action: any): State {
  return userModuleAssignmentReducer(state, action);
}

export const getData = (state: State) => state.data;
export const getLoading = (state: State) => state.loading;
export const getLoaded = (state: State) => state.loaded;
export const getFailed = (state: State) => state.failed;
