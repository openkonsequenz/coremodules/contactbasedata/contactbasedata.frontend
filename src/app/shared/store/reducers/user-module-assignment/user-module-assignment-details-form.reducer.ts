/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as userModuleAssignmentActions from '@shared/store/actions/user-module-assignment/user-module-assignment.action';
import { createFormGroupState, createFormStateReducerWithUpdate, updateGroup, FormGroupState, FormState, SetValueAction, validate } from 'ngrx-forms';
import { UserModuleAssignment } from '@shared/models';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'userModuleAssignmentDetailForm';

export const INITIAL_STATE: FormGroupState<UserModuleAssignment> = createFormGroupState<UserModuleAssignment>(FORM_ID, new UserModuleAssignment());

export const validateForm: ActionReducer<FormState<UserModuleAssignment>> = createFormStateReducerWithUpdate<UserModuleAssignment>(
  updateGroup<UserModuleAssignment>({
    modulName: validate(required),
  })
);

export function reducer(state: FormGroupState<UserModuleAssignment> = INITIAL_STATE, action: Action): FormGroupState<UserModuleAssignment> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }

  if (action.type === userModuleAssignmentActions.loadUserModuleAssignmentDetailsSuccess.type) {
    const userModuleAssignment: UserModuleAssignment = <UserModuleAssignment>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, userModuleAssignment);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}
export const getFormState = (state: FormGroupState<UserModuleAssignment>) => state;
