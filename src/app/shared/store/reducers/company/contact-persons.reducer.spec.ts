 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {companyContactPersonsReducer, INITIAL_STATE, getData, getLoading, getLoaded, getFailed, reducer} from '@shared/store/reducers/company/contact-persons.reducer';
import * as companyActions from '@shared/store/actions/company/company.action';
import { ContactPerson } from '@shared/models';

describe('CompanyContactPersonReducer', () => {

  it('should return the initial state', () => {
    const action = { type: 'NOOP' } as any;
    const result = reducer(undefined, action);

    expect(result).toBe(INITIAL_STATE);
  });

  it('should trigger loading state', () => {
    const action = companyActions.loadCompanyDetailContactPersons({payload: 'id'});
    const result = companyContactPersonsReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      loading: true,
    });
  });

  it('should trigger loaded state', () => {
    const list: companyActions.ILoadCompanyContactPersonsSuccess = {payload: [new ContactPerson()]};
    list.payload[0].firstName = 'testme';
    const action = companyActions.loadCompanyDetailContactPersonsSuccess(list);
    const result = companyContactPersonsReducer(INITIAL_STATE, action);

    expect(result.loaded).toBe(true);
  });

  it('should trigger failed state', () => {
    const error: companyActions.ILoadCompanyCommunicationsDataFail = {payload: 'err_msg'};
    const action = companyActions.loadCompanyDetailContactPersonsFail(error);
    const result = companyContactPersonsReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      failed: true,
    });
  });

  it('getData return state.data', () => {
    const state = { ...INITIAL_STATE };
    const result = getData(state);
    expect(result).toBe(state.data);
  });

  it('getLoading return state.loading', () => {
    const state = { ...INITIAL_STATE };
    const result = getLoading(state);
    expect(result).toBe(state.loading);
  });

  it('getLoaded return state.loaded', () => {
    const state = { ...INITIAL_STATE };
    const result = getLoaded(state);
    expect(result).toBe(state.loaded);
  });

  it('getFailed return state.failed', () => {
    const state = { ...INITIAL_STATE };
    const result = getFailed(state);
    expect(result).toBe(state.failed);
  });

});
