/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PageModel } from '@shared/models/page/page.model';
import * as contactsActions from '@shared/store/actions/contacts.action';
import { Contact } from '@shared/models';
import { Action, createReducer, on } from '@ngrx/store';

export interface State {
  loading: boolean;
  loaded: boolean;
  failed: boolean;
  data: PageModel<Contact>;
}

export const INITIAL_STATE: State = {
  loading: false,
  loaded: false,
  failed: false,
  data: null,
};

export const contactsPageReducer = createReducer(
  INITIAL_STATE,
  on(contactsActions.loadContactsPage, (state: State | undefined, action: any) => {
    return {
      ...state,
      loading: true,
      loaded: false,
      failed: false,
      data: null,
    };
  }),
  on(contactsActions.loadContactsPageSuccess, (state: State | undefined, action: any) => {
    return {
      ...state,
      loading: false,
      loaded: true,
      failed: false,
      data: action['payload'],
    };
  }),
  on(contactsActions.loadContactsPageFail, (state: State | undefined, action: any) => {
    return {
      ...state,
      loaded: false,
      loading: false,
      failed: true,
      data: null,
    };
  })
);

export function reducer(state: State | undefined, action: Action): State {
  return contactsPageReducer(state, action);
}

export const getData = (state: State) => state.data;
export const getLoading = (state: State) => state.loading;
export const getLoaded = (state: State) => state.loaded;
export const getFailed = (state: State) => state.failed;
