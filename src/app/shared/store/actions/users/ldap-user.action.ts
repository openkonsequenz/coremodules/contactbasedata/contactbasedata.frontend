/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { LdapUser } from '@shared/models';

export interface ILoadLdapUsersSuccess {
  payload: Array<LdapUser>;
}

export interface ILoadLdapUsersFail {
  payload: string;
}
export const loadLdapUsers = createAction('[Users] Load LdapUsers');
export const loadLdapUsersSuccess = createAction('[Users] Load LdapUsers Success', props<ILoadLdapUsersSuccess>());
export const loadLdapUsersFail = createAction('[Users] Load LdapUsers Fail', props<ILoadLdapUsersFail>());
