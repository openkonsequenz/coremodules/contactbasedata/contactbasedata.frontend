/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { KeycloakUser } from '@shared/models';

export interface ILoadKeycloakUsersSuccess {
  payload: Array<KeycloakUser>;
}

export interface ILoadKeycloakUsersFail {
  payload: string;
}
export const loadKeycloakUsers = createAction('[Users] Load KeycloakUsers');
export const loadKeycloakUsersSuccess = createAction('[Users] Load KeycloakUsers Success', props<ILoadKeycloakUsersSuccess>());
export const loadKeycloakUsersFail = createAction('[Users] Load KeycloakUsers Fail', props<ILoadKeycloakUsersFail>());
