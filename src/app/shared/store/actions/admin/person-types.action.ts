/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { PersonType } from '@shared/models';

export interface ILoadPersonTypesSuccess {
  payload: Array< PersonType>;
}
export interface ILoadPersonTypesFail {
  payload: string;
}

export const loadPersonTypes = createAction('[PersonTypes] Load');
export const loadPersonTypesSuccess = createAction(
  '[PersonTypes] Load Success',
  props<ILoadPersonTypesSuccess>());
export const loadPersonTypesFail = createAction(
  '[PersonTypes] Load Fail',
  props<ILoadPersonTypesFail>());

export const loadPersonType = createAction(
  '[PersonType Details] Load',
  props<{ payload: string }>()
);
export const loadPersonTypeSuccess = createAction(
  '[PersonType Details] Load Success',
  props<{ payload: PersonType }>()
);
export const loadPersonTypeFail = createAction(
  '[PersonType Details] Load Fail',
  props<{ payload: string }>()
);

export const savePersonType = createAction(
  '[PersonType Details] Save',
  props<{ payload: PersonType }>()
);
export const savePersonTypeSuccess = createAction(
  '[PersonType Details] Save Success',
  props<{ payload: PersonType }>()
);
export const savePersonTypeFail = createAction(
  '[PersonType Details] Save Fail',
  props<{ payload: string }>()
);

export const deletePersonType = createAction(
  '[PersonType Details] Delete',
  props<{ payload: string }>()
);
export const deletePersonTypeSuccess = createAction(
  '[PersonType Details] Delete Success'
);
export const deletePersonTypeFail = createAction(
  '[PersonType Details] Delete Fail',
  props<{ payload: string }>()
);
