/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import { ServerSideModel } from '@shared/models/server-side.model';

export abstract class BaseListSandbox extends BaseSandbox {
  protected pageSize: number = 10;
  public serversideModel: ServerSideModel = new ServerSideModel();

  public setCurrentPageNumber(pageNumber: number): void {
    this.serversideModel = { ...this.serversideModel, pageNumber: pageNumber };
  }
}
