/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class Address {
  public id: string = null;
  public contactId: string = null;
  public isMainAddress: boolean = null;
  public addressTypeId: string = null;
  public addressTypeType: string = null;
  public addressTypeDescription: string = null;
  public postcode: string = null;
  public mainAddress: string = null;
  public community: string = null;
  public communitySuffix: string = null;
  public street: string = null;
  public housenumber: string = null;
  public wgs84Zone: string = null;
  public latitude: string = null;
  public longitude: string = null;
  public urlMap: string = null;
  public note: string = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }
}
