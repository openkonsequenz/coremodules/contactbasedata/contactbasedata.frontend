/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { IconCellRendererComponent } from '@shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';
import { DateCellRendererComponent } from '@shared/components/cell-renderer/date-cell-renderer/date-cell-renderer.component';

export const USER_MODULE_ASSIGNMENT_LIST_COLDEF = [
  {
    field: 'modulName',
    headerName: 'UserModuleAssignment.ModuleName',
    sortable: true,
    flex: 2,
  },
  {
    field: 'assignmentDate',
    headerName: 'UserModuleAssignment.AssignmentDate',
    sortable: true,
    flex: 1,
    cellRenderer: DateCellRendererComponent,
  },
  {
    field: 'expiringDate',
    headerName: 'UserModuleAssignment.ExpiringDate',
    sortable: true,
    flex: 1,
    cellRenderer: DateCellRendererComponent,
  },
  {
    field: 'deletionLockUntil',
    headerName: 'UserModuleAssignment.DeletionLockUntil',
    sortable: true,
    flex: 1,
    cellRenderer: DateCellRendererComponent,
  },
  {
    field: 'assignmentNote',
    headerName: 'UserModuleAssignment.AssignmentNote',
    sortable: true,
    flex: 3,
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    minWidth: 110,
    maxWidth: 110,
    lockPosition: true,
    sortable: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRenderer: IconCellRendererComponent,
  },
];
