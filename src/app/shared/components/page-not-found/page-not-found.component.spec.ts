/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { PageNotFoundComponent } from '@app/shared/components/page-not-found/page-not-found.component';

describe('PageNotFoundComponent', () => {
  let component: PageNotFoundComponent;
  let location: any;

  beforeEach(async(() => {
    location = { back() {} } as any;
  }));

  beforeEach(() => {
    component = new PageNotFoundComponent(location);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call back', () => {
    const spy = spyOn(location, 'back');
    component.goBack();
    expect(spy).toHaveBeenCalled();
  });
});
