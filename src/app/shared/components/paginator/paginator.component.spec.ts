/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { PaginatorComponent } from '@shared/components/paginator/paginator.component';

describe('PaginatorComponent', () => {
  let component: PaginatorComponent;

  beforeEach(async(() => {}));

  beforeEach(() => {
    component = new PaginatorComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component).toBeDefined();
  });

  it('checks if setter and getter visibleItemsAmount is called and works', () => {
    component.visibleItemsAmount = 10;
    expect(component.visibleItemsAmount).toBe(10);
  });

  it('checks if setter and getter totalPages is called and works', () => {
    component.totalPages = 15;
    expect(component.totalPages).toBe(15);
  });

  it('return totalPages if totalPages are lesser then visibleItemsAmount', () => {
    component.visibleItemsAmount = 15;
    component.totalPages = 10;
    expect(component.visibleItemsAmount).toBe(10);
  });

  it('check if isPagingActive works', () => {
    component.totalPages = 1;
    expect(component.isPagingActive).toBeFalsy();
    component.totalPages = 2;
    expect(component.isPagingActive).toBeTruthy();
  });

  it('check if setNextPage works', () => {
    component.totalPages = 5;
    expect(component.pageEventItems[component.currentPageIndex].pageIndex).toBe(1);
    component.setNextPage();
    expect(component.pageEventItems[component.currentPageIndex].pageIndex).toBe(2);
  });

  it('check if setPreviousPage works', () => {
    component.totalPages = 5;
    component.setNextPage();
    component.setPreviousPage();
    expect(component.pageEventItems[component.currentPageIndex].pageIndex).toBe(1);
  });

  it('check if setPreviousPage works on page 1', () => {
    component.totalPages = 5;
    expect(component.pageEventItems[component.currentPageIndex].pageIndex).toBe(1);
    component.setPreviousPage();
    expect(component.pageEventItems[component.currentPageIndex].pageIndex).toBe(1);
  });

  xit('check if setLastPage works', () => {
    component.totalPages = 10;
    component.setLastPage();
    // console.log(component.currentPageIndex);
    // console.log(component.pageEventItems[0]);
    // console.log(component.pageEventItems[component.currentPageIndex].pageIndex);
    expect(component.currentPageIndex).toBe(5);
  });

  it('check if setFirstPage works on first page', () => {
    component.totalPages = 10;
    component.setFirstPage();
    expect(component.pageEventItems[component.currentPageIndex].pageIndex).toBe(1);
  });

  it('check if setFirstPage works on <> first page', () => {
    component.totalPages = 10;
    component.setNextPage();
    component.setFirstPage();
    expect(component.pageEventItems[component.currentPageIndex].pageIndex).toBe(1);
  });

  it('check if isLastPageLastIcon works', () => {
    component.totalPages = 2;
    component.setNextPage();
    let isLastPageLastIcon = component.isLastPageLastIcon();
    expect(isLastPageLastIcon).toBeTruthy();
  });

  xit('check if isLastPageLastIcon works', () => {
    component.totalPages = 6;
    component.setNextPage();
    let isLastPageLastIcon = component.isLastPageLastIcon();
    // console.log(component.pageEventItems[component.currentPageIndex].pageIndex);
    expect(isLastPageLastIcon).toBeFalsy();
  });
});
