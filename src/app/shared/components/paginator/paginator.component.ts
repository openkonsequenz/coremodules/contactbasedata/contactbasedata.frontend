/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PageEvent } from '@shared/models/PageEvent';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent {
  private _totalPages: number;
  private _visibleItemsAmount: number = 5;

  @Input()
  length: number;

  @Input()
  pageSize: number;

  @Input()
  public set currentPageNumber(pageNumber: number) {
    this._setActivePage({ pageIndex: pageNumber });
  }

  @Input()
  public set visibleItemsAmount(visibleItemsAmount: number) {
    this._visibleItemsAmount = visibleItemsAmount;
  }

  public get visibleItemsAmount(): number {
    if (this.totalPages < this._visibleItemsAmount) {
      return this.totalPages;
    } else {
      return this._visibleItemsAmount;
    }
  }

  @Input()
  public set totalPages(totalPages: number) {
    this._totalPages = totalPages;
    this._init(1);
  }

  @Input()
  hidePageSize: boolean;

  @Output()
  page: EventEmitter<PageEvent> = new EventEmitter();

  public get totalPages(): number {
    return this._totalPages;
  }

  public pageEventItems: PageEvent[];

  public activeEventItem: PageEvent;

  public currentPageIndex: number = 0;

  public get isPagingActive(): boolean {
    return this.totalPages > 1;
  }

  public setSelectedPage(pageEvent: PageEvent): void {
    if (this._setPagesRange(pageEvent)) {
      return;
    }
    this._setActivePage(pageEvent);
    this.page.emit(pageEvent);
  }

  public setNextPage(): void {
    if (this.isLastPage()) {
      return;
    }
    const nextActiveItem = this.pageEventItems[this.currentPageIndex + 1];
    if (this._setPagesRange(nextActiveItem)) {
      return;
    }
    this.setSelectedPage(nextActiveItem);
  }

  public setLastPage(): void {
    if (this.isLastPage()) {
      return;
    }
    this._init(this._totalPages);

    this.setSelectedPage({ pageIndex: this._totalPages });
  }

  public setPreviousPage(): void {
    if (this.isFirstPage()) {
      return;
    }
    const previousActiveItem = this.pageEventItems[this.currentPageIndex - 1];
    if (this._setPagesRange(previousActiveItem, false)) {
      return;
    }
    this.setSelectedPage(previousActiveItem);
  }

  public setFirstPage(): void {
    if (this.isFirstPage()) {
      return;
    }
    this._init(1);

    this.setSelectedPage({ pageIndex: 1 });
  }

  public isLastPage(): boolean {
    const activeItem = this.pageEventItems[this.currentPageIndex];
    return !!activeItem && activeItem.pageIndex == this.totalPages;
  }

  public isLastPageLastIcon(): boolean {
    const activeItem = this.pageEventItems[this.currentPageIndex];
    return !!activeItem && activeItem.pageIndex >= this.totalPages - this.visibleItemsAmount;
  }

  public isFirstPage(): boolean {
    const activeItem = this.pageEventItems[this.currentPageIndex];
    return !!activeItem && activeItem.pageIndex === 1;
  }

  private _setActivePage(pageEvent: PageEvent): void {
    this.currentPageIndex = this.pageEventItems.findIndex((pev: PageEvent) => pev.pageIndex === pageEvent.pageIndex);
    this.activeEventItem = pageEvent;
  }

  private _init(index: number, setNextRange: boolean = true) {
    this.currentPageIndex = 0;
    if (index === this.totalPages) {
      index = index - (this.visibleItemsAmount - 1);
    } else {
      index = setNextRange ? index : index - (this.visibleItemsAmount - 2);
    }

    this.pageEventItems = new Array<PageEvent>();
    for (let i = 0; this.visibleItemsAmount > i; i++) {
      this.pageEventItems.push({ pageIndex: index++ });
    }
    this._setActivePage(this.pageEventItems[this.currentPageIndex]);
  }

  private _setPagesRange(pageEvent: PageEvent, nextPage: boolean = true) {
    if (!pageEvent) {
      const currentIndex = nextPage ? this.pageEventItems[this.currentPageIndex].pageIndex + 2 : this.pageEventItems[this.currentPageIndex].pageIndex - 2;
      this._init(currentIndex, nextPage);
      return true;
    }
  }
}
