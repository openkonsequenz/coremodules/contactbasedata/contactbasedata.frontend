import { UserModuleAssignment } from '@shared/models';
import { UserModuleAssignmentSandBox } from '@app/shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
import { BaseList } from '@shared/components/base-components/base.list';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { USER_MODULE_ASSIGNMENT_LIST_COLDEF } from '@app/shared/components/column-definitions/user-module-assignment-data-list-column-definition';
import { Globals } from '@shared/constants/globals';

@Component({
  selector: 'app-module-assignment-data-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class UserModuleAssignmentDataListComponent extends BaseList {
  @Output() userModuleAssignmentDetailsLoaded: EventEmitter<UserModuleAssignment> = new EventEmitter();
  @Output() createNewUserModuleAssignment: EventEmitter<string> = new EventEmitter();

  public columnDefinition = USER_MODULE_ASSIGNMENT_LIST_COLDEF;

  constructor(public userModuleAssignmentSandBox: UserModuleAssignmentSandBox) {
    super();
  }

  ngOnInit() {
    this.gridOptions = {
      ...this.gridOptions,
      localeText: Globals.LOCALE_TEXT,
    };
    this.gridOptions.context = {
      ...this.gridOptions.context,
      icons: { edit: true, delete: true },
    };

    this.gridOptions.context.eventSubject.subscribe(event => {
      if (event.type === 'edit' || event.type === 'readonly') {
        this.userModuleAssignmentDetailsLoaded.emit(event.data);
      }
      if (event.type === 'delete') {
        this.userModuleAssignmentSandBox.deleteUserModuleAssignment(event.data);
      }
    });
  }

  public createNewUserModuleAssignmentForm() {
    this.userModuleAssignmentSandBox.clearUserModuleAssignmentData();
    this.createNewUserModuleAssignment.emit(null);
    this.userModuleAssignmentSandBox.userModuleAgApi.setDomLayout('normal');
  }

  onGridReady(params) {
    this.userModuleAssignmentSandBox.userModuleAgApi = params.api;
  }
}
