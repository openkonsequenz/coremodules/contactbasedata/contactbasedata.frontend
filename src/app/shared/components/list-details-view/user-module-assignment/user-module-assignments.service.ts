/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ExternalPerson, InternalPerson, UserModuleAssignment } from '@shared/models';
import { Address, CommunicationsData } from '@shared/models';
import { Injectable } from '@angular/core';
import { PageModel } from '@shared/models/page/page.model';
import { UserModuleType, UserModuleTypeDTO } from '@app/shared/models/user-module-assignment/user-module-type.model';

/**
 * Used to retrieve persons
 *
 * @export
 * @class PersonsService
 */
@Injectable()
export class UserModuleAssignmentsService {
  /**
   * Transforms user module assignments received from the API into instance of 'userModuleAssignment[]'
   *
   * @param userModuleAssignments
   */
  static userModuleAssignmentsAdapter(userModuleAssignments: any): Array<UserModuleAssignment> {
    return userModuleAssignments.map(userModuleAssignment => new UserModuleAssignment(userModuleAssignment));
  }

  /**
   * Transforms user module assignment details recieved from the API into instance of 'userModuleAssignment'
   *
   * @param response
   */
  static userModuleAssignmentDetailsAdapter(userModuleAssignment: UserModuleAssignment): UserModuleAssignment {
    return new UserModuleAssignment(userModuleAssignment);
  }

  /**
   * Transforms user module type recieved from the API into instance of 'userModuleType'
   *
   * @param response
   */
  static userModuleTypesAdapter(response: UserModuleTypeDTO[]): UserModuleType[] {
    return response.map(userModuleType => new UserModuleType({ id: userModuleType.name, color: userModuleType.color }));
  }
}
