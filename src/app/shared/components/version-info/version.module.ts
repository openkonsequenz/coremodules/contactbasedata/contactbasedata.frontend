/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '@shared/components';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '@shared/directives';
import { ContainersModule } from '@shared/containers';
import { EffectsModule } from '@ngrx/effects';
import { VersionApiClient } from './version-api-client';
import { VersionEffects } from '@app/shared/store/effects/version/version.effect';
import { VersionInfo } from './version-info.component';
import { VersionSandbox } from './version-info.sandbox';


@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    TranslateModule,
    DirectivesModule,
    RouterModule,
    FormsModule,
    ContainersModule,
    EffectsModule.forFeature([VersionEffects]),
  ],
  providers: [VersionSandbox, VersionApiClient],
})
export class VersionModule {}
