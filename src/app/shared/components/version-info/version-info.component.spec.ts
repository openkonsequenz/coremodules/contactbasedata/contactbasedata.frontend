 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { VersionInfo } from '@shared/components/version-info/version-info.component';

describe('VersionInfo', () => {
  let component: VersionInfo;
  let versionSandbox: any;

  beforeEach(async(() => {
  }));

  beforeEach(() => {
    versionSandbox = {
      loadVersion(){}
    } as any;
    component = new VersionInfo(versionSandbox);
  });

  it('should create VersionInfo', () => {
    expect(component).toBeTruthy();
  });

  it('should loadVersion on init', () => {
    const spy = spyOn(versionSandbox, 'loadVersion');
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });
});
