/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { Component, OnInit } from '@angular/core';
import { VersionSandbox } from './version-info.sandbox';

@Component({
  selector: 'app-version-info',
  templateUrl: './version-info.component.html',
  styleUrls: ['./version-info.component.css'],
})
export class VersionInfo implements OnInit {
  constructor(public versionSandbox: VersionSandbox) {}

  ngOnInit() {
    this.versionSandbox.loadVersion();
  }
}
