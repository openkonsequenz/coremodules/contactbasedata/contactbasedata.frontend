/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpService } from './http.service';
import { HttpResponseHandler } from './http-response-handler.service';

@NgModule({
  imports: [CommonModule],
})
export class HttpServiceModule {
  static forRoot(): ModuleWithProviders<HttpServiceModule> {
    return {
      ngModule: HttpServiceModule,

      providers: [HttpService, HttpResponseHandler],
    };
  }
}
