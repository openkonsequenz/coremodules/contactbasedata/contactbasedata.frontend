/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
// import { Headers, URLSearchParams, RequestOptions, Request, Response } from '@angular/http';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

export function methodBuilder(method: string) {
  return function (url: string) {
    return function (target: HttpService, propertyKey: string, descriptor: any) {
      const pPath = target[`${propertyKey}_Path_parameters`],
        pQuery = target[`${propertyKey}_Query_parameters`],
        pBody = target[`${propertyKey}_Body_parameters`],
        pHeader = target[`${propertyKey}_Header_parameters`];

      descriptor.value = function (...args: any[]) {
        const body: string = createBody(pBody, descriptor, args);
        const resUrl: string = createPath(url, pPath, args);
        // const search: URLSearchParams = createQuery(pQuery, args);
        const params: HttpParams = createQuery(pQuery, args);
        const headers: HttpHeaders = createHeaders(pHeader, descriptor, this.getDefaultHeaders(), args);

        // Request options
        const options = {
          body,
          headers,
          params,
          // observe: 'events',
          responseType: 'json',
        };

        // const req = new Request(options);

        // intercept the request
        // this.requestInterceptor(req);
        // make the request and store the observable for later transformation
        let observable: Observable<Response> = this.httpClient.request(method, this.getBaseUrl() + resUrl, options);

        // intercept the response
        observable = this.responseInterceptor(observable, descriptor.adapter);

        return observable;
      };

      return descriptor;
    };
  };
}

export function paramBuilder(paramName: string) {
  return function (key: string) {
    return function (target: HttpService, propertyKey: string | symbol, parameterIndex: number) {
      const metadataKey = `${String(propertyKey)}_${paramName}_parameters`;
      const paramObj: any = {
        key: key,
        parameterIndex: parameterIndex,
      };

      if (Array.isArray(target[metadataKey])) {
        target[metadataKey].push(paramObj);
      } else {
        target[metadataKey] = [paramObj];
      }
    };
  };
}

function createBody(pBody: Array<any>, descriptor: any, args: Array<any>): string {
  if (descriptor.isFormData) {
    return args[0];
  }
  return pBody ? JSON.stringify(args[pBody[0].parameterIndex]) : null;
}

function createPath(url: string, pPath: Array<any>, args: Array<any>): string {
  let resUrl: string = url;

  if (pPath) {
    for (const k in pPath) {
      if (pPath.hasOwnProperty(k)) {
        resUrl = resUrl.replace('{' + pPath[k].key + '}', args[pPath[k].parameterIndex]);
      }
    }
  }

  return resUrl;
}

// function createQuery(pQuery: any, args: Array<any>): URLSearchParams {
//   const search = new URLSearchParams();

//   if (pQuery) {
//     pQuery
//       .filter(p => args[p.parameterIndex]) // filter out optional parameters
//       .forEach(p => {
//         const key = p.key;
//         let value = args[p.parameterIndex];
//         // if the value is a instance of Object, we stringify it
//         if (value instanceof Object) {
//           value = JSON.stringify(value);
//         }
//         search.set(encodeURIComponent(key), value);
//       });
//   }

//   return search;
// }

function createQuery(pQuery: any, args: Array<any>): HttpParams {
  let param = new HttpParams();
  if (pQuery) {
    pQuery
      .filter(p => args[p.parameterIndex]) // filter out optional parameters
      .forEach(p => {
        const key = p.key;
        let value = args[p.parameterIndex];
        // if the value is a instance of Object, we stringify it
        if (value instanceof Object) {
          value = JSON.stringify(value);
        }
        param = param.append(encodeURIComponent(key), value);
      });
  }

  return param;
}

function createHeaders(pHeader: any, descriptor: any, defaultHeaders: any, args: Array<any>): HttpHeaders {
  let headers = new HttpHeaders(defaultHeaders);
  const authorization = 'Authorization';
  if (headers.has(authorization)) {
    headers = headers.delete(authorization);
  }
  headers = headers.append(authorization, localStorage.getItem('token'));
  // set method specific headers
  for (const k in descriptor.headers) {
    if (descriptor.headers.hasOwnProperty(k)) {
      if (headers.has(k)) {
        headers = headers.delete(k);
      }
      headers = headers.append(k, descriptor.headers[k]);
    }
  }

  // set parameter specific headers
  if (pHeader) {
    for (const k in pHeader) {
      if (pHeader.hasOwnProperty(k)) {
        if (headers.has(k)) {
          headers = headers.delete(k);
        }
        headers = headers.append(pHeader[k].key, args[pHeader[k].parameterIndex]);
      }
    }
  }

  return headers;
}
