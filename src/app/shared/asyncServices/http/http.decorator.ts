/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { HttpService, MediaType } from './http.service';
import { methodBuilder, paramBuilder } from './utils.service';

export class HttpRequestMethods {
  static readonly HEAD = 'HEAD';
  static readonly GET = 'GET';
  static readonly POST = 'POST';
  static readonly PUT = 'PUT';
  static readonly DELETE = 'DELETE';
  static readonly UPLOAD = 'UPLOAD';
}
/* *********************************************
 * Class decorators
 * *********************************************/

/**
 * Set the base URL of REST resource
 * @param {String} url - base URL
 */
export function BaseUrl(url: string) {
  return function <TFunction extends Function>(Target: TFunction): TFunction {
    Target.prototype.getBaseUrl = () => url;
    return Target;
  };
}

/**
 * Set default headers for every method of the HttpService
 * @param {Object} headers - deafult headers in a key-value pair
 */
export function DefaultHeaders(headers: any) {
  return function <TFunction extends Function>(Target: TFunction): TFunction {
    Target.prototype.getDefaultHeaders = () => headers;
    return Target;
  };
}

/* *********************************************
 * Method decorators
 * *********************************************/

/**
 * GET method
 * @param {string} url - resource url of the method
 */
export const GET = methodBuilder(HttpRequestMethods.GET);
/**
 * POST method
 * @param {string} url - resource url of the method
 */
export const POST = methodBuilder(HttpRequestMethods.POST);
/**
 * PUT method
 * @param {string} url - resource url of the method
 */
export const PUT = methodBuilder(HttpRequestMethods.PUT);
/**
 * DELETE method
 * @param {string} url - resource url of the method
 */
export const DELETE = methodBuilder(HttpRequestMethods.DELETE);
/**
 * HEAD method
 * @param {string} url - resource url of the method
 */
export const HEAD = methodBuilder(HttpRequestMethods.HEAD);

/**
 * Set custom headers for a REST method
 * @param {Object} headersDef - custom headers in a key-value pair
 */
export function Headers(headersDef: any) {
  return function (target: HttpService, propertyKey: string, descriptor: any) {
    descriptor.headers = headersDef;
    return descriptor;
  };
}

/**
 * Defines the media type(s) that the methods can produce
 * @param MediaType producesDef - MediaType to be sent
 */
export function Produces(producesDef: MediaType) {
  return function (target: HttpService, propertyKey: string, descriptor: any) {
    descriptor.isJSON = producesDef === MediaType.JSON;
    descriptor.isFormData = producesDef === MediaType.FORM_DATA;
    return descriptor;
  };
}

/**
 * Defines the adatper function to modify the API response suitable for the app
 * @param TFunction adapterFn - function to be called
 */
export function Adapter(adapterFn: Function) {
  return function (target: HttpService, propertyKey: string, descriptor: any) {
    descriptor.adapter = adapterFn || null;
    return descriptor;
  };
}

/* *********************************************
 * Parameter decorators
 * *********************************************/

/**
 * Path constiable of a method's url, type: string
 * @param {string} key - path key to bind value
 */
export const Path = paramBuilder('Path');
/**
 * Query value of a method's url, type: string
 * @param {string} key - query key to bind value
 */
export const Query: any = paramBuilder('Query');
/**
 * Body of a REST method, type: key-value pair object
 * Only one body per method!
 */
export const Body: any = paramBuilder('Body');
/**
 * Custom header of a REST method, type: string
 * @param {string} key - header key to bind value
 */
export const Header: any = paramBuilder('Header');
