 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ConfigService } from 'app/app-config.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { LayoutSandbox } from '@shared/containers/layout/layout.sandbox';
import { Store } from '@ngrx/store';
import { User } from '@shared/models/user';
import * as store from '@shared/store';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.container.html',
  styleUrls: ['./layout.container.scss']
})
export class LayoutContainerComponent implements OnInit, OnDestroy {
  // public userImage = '';
  // public userName = 'Test';

  public user$: Observable<User> = this.appState$.select(store.getUser);

  private assetsFolder: string;

  private subscriptions: Array<Subscription> = [];

  constructor(
    private configService: ConfigService,
    protected appState$: Store<store.State>,
    public layoutSandbox: LayoutSandbox
  ) {
    this.assetsFolder = this.configService.get('paths').userImageFolder;
  }

  ngOnInit() {
    this.registerEvents();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  private registerEvents() {
    // Subscribes to user changes
  }
}
