/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Directive, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { AgGridEvent, Column, GridApi, GridOptions } from 'ag-grid-community';

const HEADER_HEIGHT = 56;
// tslint:disable-next-line: directive-selector
@Directive({ selector: 'ag-grid-angular[autoResizeColumns]' })
export class AutoResizeColumnsDirective implements OnInit {
  @Input()
  public sizeCellsToContent = false;
  @Input()
  public tableWidthBuffer = 0;
  private _gridWidth: number;
  private _gridHeight: number;
  private _rowHeight: any;

  @Output() autoResizeDone = new EventEmitter();
  constructor(public agGrid: AgGridAngular) {}

  ngOnInit() {
    this._autoResizeColumns();
  }

  private _autoResizeColumns() {
    const gridOptions: GridOptions = {
      ...this.agGrid.gridOptions,
      suppressColumnVirtualisation: true,
      onModelUpdated: (event: AgGridEvent) => this._onGridViewRendered(event),
      onGridSizeChanged: (event: any) => {
        this._gridWidth = event.clientWidth;
        this._gridHeight = event.clientHeight - HEADER_HEIGHT;
        this._onGridViewRendered(event);
      },
      onColumnVisible: (event: any) => {
        this._onGridViewRendered(event);
      },
      getRowHeight: params => {
        this._rowHeight = params && params.node.rowHeight;
        return this._rowHeight;
      },
    };
    this.agGrid.gridOptions = gridOptions;
  }

  private _onGridViewRendered(event: AgGridEvent) {
    const columnApi: GridApi = event.api;
    let totalColumnsWidth = 0;
    let allColumns: Column[] = [];
    let allOffsetColumns: Column[] = [];
    let divider = 1;
    let rowsHeight = 0;
    if (!this._gridWidth) {
      return;
    }
    rowsHeight = !!this.agGrid.rowData ? this._rowHeight * this.agGrid.rowData.length : 0;

    allColumns = columnApi.getAllGridColumns() || allColumns;

    allColumns = allColumns.filter((column: Column) => column.isVisible());

    columnApi.autoSizeColumns(allColumns, true);

    columnApi.sizeColumnsToFit(this._gridWidth - (rowsHeight > this._gridHeight ? 15 : this.tableWidthBuffer + 2));

    if (!this.sizeCellsToContent) {
      return;
    }
    allOffsetColumns = allColumns.filter((column: Column) => !column.getMaxWidth() && column.isVisible());

    allColumns.forEach((column: Column) => {
      totalColumnsWidth += column.getActualWidth();
    });
    divider = allOffsetColumns.length || 1;

    if (totalColumnsWidth < this._gridWidth) {
      const columnOffset: number = (this._gridWidth - 3 - totalColumnsWidth) / divider;
      allOffsetColumns.forEach((column: Column) => {
        columnApi.setColumnWidth(column.getId(), column.getActualWidth() + columnOffset);
      });
    }
    this.autoResizeDone.emit(true);
  }
}
