/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TranslateColumnDefinitionsDirective } from './translate-column-definitions.directive';

describe('TranslateHeadersDirective', () => {
  it('should create an instance', () => {
    const directive = new TranslateColumnDefinitionsDirective(null, null);
    expect(directive).toBeTruthy();
  });
});
