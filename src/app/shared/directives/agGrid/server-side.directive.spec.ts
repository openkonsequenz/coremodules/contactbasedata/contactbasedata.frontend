/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ServerSideDirective } from '@shared/directives/agGrid/server-side.directive';

describe('ServerSideDirective', () => {
  it('should create an instance', () => {
    const directive = new ServerSideDirective(null, null, null, null, null);
    expect(directive).toBeTruthy();
  });
});
